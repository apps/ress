﻿CREATE TABLE [dbo].[asset]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY,
	[asset_type_id] INT NOT NULL, 
    [full_name] NVARCHAR(255) NOT NULL, 
    [abbrev_name] NVARCHAR(255) NOT NULL, 
    [location] NVARCHAR(255) NULL, 
    [descr] NVARCHAR(MAX) NULL, 
    [cover_image_id] INT NULL, 
    [url] VARCHAR(255) NOT NULL, 
    [o365_email] VARCHAR(255) NULL, 
    [o365_ics] VARCHAR(255) NULL, 
    [is_active] BIT NOT NULL DEFAULT 1, 
    [can_reserve] BIT NOT NULL DEFAULT 1, 
    [add_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    [update_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    [show_details] BIT NOT NULL DEFAULT 1,  
    CONSTRAINT [FK_asset_asset_type] FOREIGN KEY ([asset_type_id]) REFERENCES [asset_type]([id]), 
)

GO

CREATE UNIQUE INDEX [UQ_asset_url] ON [dbo].[asset] ([url])
