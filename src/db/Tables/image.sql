﻿CREATE TABLE [dbo].[image]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [guid] UNIQUEIDENTIFIER NOT NULL DEFAULT newid(), 
    [filename] VARCHAR(100) NULL, 
    [width] SMALLINT NULL, 
    [height] SMALLINT NULL, 
    [mime_type] VARCHAR(255) NOT NULL, 
    [add_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    [subfolder] VARCHAR(255) NULL, 
)
