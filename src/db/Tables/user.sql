﻿CREATE TABLE [dbo].[user]
(
    [id]     INT           IDENTITY  NOT NULL,
    [unique_id] VARCHAR(50) NOT NULL, 
    [username]    VARCHAR (50)  NOT NULL,
    [first_name]  NVARCHAR (50)  NULL,
    [last_name]   NVARCHAR (50)  NULL,
    [email]       VARCHAR (50)  NULL,
    [add_time]      DATETIME2 (3) DEFAULT (sysutcdatetime()) NOT NULL,
    [lastsync_time] DATETIME2 (3) DEFAULT (sysutcdatetime()) NOT NULL, 
    
    CONSTRAINT [PK_users] PRIMARY KEY ([id]),
)
