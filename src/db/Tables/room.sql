﻿CREATE TABLE [dbo].[room]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[room_type_id] INT NOT NULL, 
    [url] VARCHAR (50)   NOT NULL,
    [o365_email] VARCHAR(255) NOT NULL, 
    [o365_ics] VARCHAR(500) NULL, 
    [room_number] NVARCHAR(50) NOT NULL, 
    [building_name] NVARCHAR(255) NOT NULL, 
    [display_name] NVARCHAR(255) NULL, 
    [building_number] NVARCHAR(50) NULL, 
    [capacity] NVARCHAR(50) NULL, 
    [descr] NVARCHAR(MAX) NULL,
    [map_image_id] INT NULL, 
    [cover_image_id] INT NULL, 
    [is_active] BIT NOT NULL DEFAULT 1, 
    [can_reserve] BIT NOT NULL DEFAULT 1, 
    [add_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    [update_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    [show_details] BIT NOT NULL DEFAULT 1, 
    CONSTRAINT [FK_room_room_type] FOREIGN KEY ([room_type_id]) REFERENCES [room_type]([id]), 
    CONSTRAINT [FK_room_image] FOREIGN KEY ([map_image_id]) REFERENCES [image]([id]), 
    CONSTRAINT [FK_room_image2] FOREIGN KEY ([cover_image_id]) REFERENCES [image]([id]),

)

GO

CREATE UNIQUE INDEX [UQ_room_url] ON [dbo].[room] ([url])
