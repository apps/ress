﻿CREATE TABLE [dbo].[room_type]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [descr] NVARCHAR(50) NOT NULL, 
    [sort_index] SMALLINT NOT NULL DEFAULT 10, 
    [is_active] BIT NOT NULL DEFAULT 1, 
    [add_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime(), 
    [update_time] DATETIME2(3) NOT NULL DEFAULT sysutcdatetime()
)
