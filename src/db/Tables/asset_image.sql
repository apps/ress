﻿CREATE TABLE [dbo].[asset_image]
(
	[asset_id] INT NOT NULL, 
    [image_id] INT NOT NULL,
	[sort_index] SMALLINT NOT NULL DEFAULT 10, 
    CONSTRAINT [PK_asset_image] PRIMARY KEY CLUSTERED 
(
	[asset_id] ASC,
	[image_id] ASC
), 
    CONSTRAINT [FK_asset_image_asset] FOREIGN KEY ([asset_id]) REFERENCES [asset]([id]), 
	CONSTRAINT [FK_asset_image_image] FOREIGN KEY ([image_id]) REFERENCES [image]([id]), 
)
