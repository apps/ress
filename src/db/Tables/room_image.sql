﻿CREATE TABLE [dbo].[room_image]
(
	[room_id] INT NOT NULL, 
    [image_id] INT NOT NULL,
	[sort_index] SMALLINT NOT NULL, 
    CONSTRAINT [PK_room_image] PRIMARY KEY CLUSTERED 
(
	[room_id] ASC,
	[image_id] ASC
), 
    CONSTRAINT [FK_room_image_asset] FOREIGN KEY ([room_id]) REFERENCES [room]([id]), 
	CONSTRAINT [FK_room_image_image] FOREIGN KEY ([image_id]) REFERENCES [image]([id]), 
)
