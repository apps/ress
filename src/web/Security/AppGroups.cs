﻿namespace AAE.RESS.Web.Security
{
    /// <summary>
    /// Group definitions for this app
    /// </summary>
    public class AppGroups
    {
        public AppGroups(string sysadmin, string resource_editors)
        {
            SysAdmin = sysadmin;
            ResourceEditors = resource_editors;
        }


        public string SysAdmin { get; private set; }
        public string ResourceEditors { get; private set; }

    }
}
