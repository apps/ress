﻿import Vue from 'vue'
import moment from 'moment'
//import { FormDatepickerPlugin } from 'bootstrap-vue'
//Vue.use(FormDatepickerPlugin)

new Vue({
    el: '#app',
    data: {
        title: '',
        start: '',
        end: '',
        isAllDay: false,
        descr: '',
        frequency: '',
        recurInterval: 0,
        byMonthDay: 0,
        byDayInterval: 0,
        byDay: '',
        repeatUntil: ''
    },
    components: {
        //FormDatepickerPlugin
    },
    methods: {
        dateFormatter(date) {
            return moment(date).format('M/d/YYYY')
        }
    }
});
