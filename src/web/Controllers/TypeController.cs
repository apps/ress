﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AAE.RESS.Application;
using AAE.RESS.Interfaces;
using AAE.RESS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AAE.RESS.Web.Controllers
{
    public class TypeController : Controller
    {
        IResourceService _resourceService;

        public TypeController(IResourceService resourceService)
        {
            _resourceService = resourceService ?? throw new ArgumentNullException(nameof(resourceService));
        }

        [HttpGet("{type:regex(rooms|equipment)}")]
        public async Task<IActionResult> Index(string type)
        {
            ResourceType rType;
            string rTypeText = string.Empty;
            if (type == "rooms")
            {
                rType = ResourceType.Room;
                rTypeText = "Rooms";
            }
            else if (type == "equipment")
            {
                rType = ResourceType.Equipment;
                rTypeText = "Equipment";
            }
            else
                throw new ArgumentOutOfRangeException(nameof(type));

            var resources = await _resourceService.GetAsync(rType);

            var vm = new TypeViewModel() 
            { 
                ResourceType = rType,
                ResourceTypeText = rTypeText,
                Resources = resources
            };

            return View(vm);
        }
    }
}