using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AAE.RESS.Application;
using AAE.RESS.Interfaces;
using AAE.RESS.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace AAE.RESS.Web.Controllers
{
    public class InfoController : Controller
    {
        private readonly IRoomQueries _roomQueries;
        private readonly IEquipmentQueries _equipmentQueries;

        public InfoController(IRoomQueries roomQueries, IEquipmentQueries equipmentQueries)
        {
            _roomQueries = roomQueries ?? throw new ArgumentNullException(nameof(roomQueries));
            _equipmentQueries = equipmentQueries ?? throw new ArgumentNullException(nameof(equipmentQueries));
        }

        [HttpGet("{type:regex(rooms|equipment)}/{url}")]
        public async Task<IActionResult> Index(string type, string url)
        {
            if (type == "rooms")
            {
                var room = await _roomQueries.GetAsync(url);
                if (room == null)
                    return NotFound();
                var vm = new InfoRoomViewModel() { Room = room };
                return View("Room", vm);
            }
            else if (type == "equipment")
            {
                var equipment = await _equipmentQueries.GetAsync(url);
                if (equipment == null)
                    return NotFound();
                var vm = new InfoEquipmentViewModel() { Equipment = equipment };
                return View("Equipment", vm);
            }
            else
                return NotFound();

        }
    }
}
