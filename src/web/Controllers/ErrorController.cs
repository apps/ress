﻿using Microsoft.AspNetCore.Mvc;

namespace AAE.RESS.Web.Controllers
{
    [Route("~/error")]
    public class ErrorController : Controller
    {
        [HttpGet("noemail")]
        public IActionResult NoEmail()
        {
            return View();
        }
    }
}
