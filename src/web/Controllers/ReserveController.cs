﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AAE.Identity;
using AAE.RESS.Application;
using AAE.RESS.Application.Reserve;
using AAE.RESS.Interfaces;
using AAE.RESS.Security.Identity;
using AAE.RESS.Web.ViewModels;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AAE.RESS.Web.Controllers
{
    public class ReserveController : Controller
    {
        private readonly IResourceService _resourceService;
        private readonly IReservationProcessor _reservationProcessor;
        private readonly IAuthorizationService _authorizationService;

        public ReserveController(IResourceService resourceService, IAuthorizationService authorizationService, IReservationProcessor reservationProcessor)
        {
            _resourceService = resourceService ?? throw new ArgumentNullException(nameof(resourceService));
            _reservationProcessor = reservationProcessor ?? throw new ArgumentNullException(nameof(reservationProcessor));
            _authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="url"></param>
        /// <remarks>requireSession must be enabled in shibboleth2.xml on webserver.  Path is hardcoded, so it must be changed if route attributes are changed</remarks>
        [Authorize]
        [HttpGet("{type:regex(rooms|equipment)}/{url}/reserve")]
        public async Task<IActionResult> Index(string type, string url)
        {
            // does the user have an email?  It is required.
            // FERPA students will not
            var currentUser = new RESSUser(HttpContext.User);
            if (currentUser.Email == null)
                return RedirectToAction("NoEmail", "Error");


            ResourceType rType;
            if (type == "rooms")
                rType = ResourceType.Room;
            else if (type == "equipment")
                rType = ResourceType.Equipment;
            else
                throw new ArgumentOutOfRangeException(nameof(type));


            var resource = await _resourceService.GetAsync(url, rType);

            var authorizationResult = await _authorizationService.AuthorizeAsync(User, resource, "ResourceReserve");
            if (authorizationResult.Succeeded)
            {
                var vm = new ReserveViewModel(resource);

                return View(vm);
            }
            else
            {
                return new ForbidResult();
            }
        }

        [HttpGet("{type:regex(rooms|equipment)}/{url}/success")]
        public async Task<IActionResult> Success(string type, string url)
        {
            ResourceType rType;
            if (type == "rooms")
                rType = ResourceType.Room;
            else if (type == "equipment")
                rType = ResourceType.Equipment;
            else
                throw new ArgumentOutOfRangeException(nameof(type));


            var resource = await _resourceService.GetAsync(url, rType);

            var vm = new ReserveViewModel(resource);

            return View(vm);
        }

        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPost("{type:regex(rooms|equipment)}/{url}/reserve")]
        public async Task<IActionResult> Reserve(string type, string url, [FromBody] ReserveInput input)
        {
            ResourceType rType;
            if (type == "rooms")
                rType = ResourceType.Room;
            else if (type == "equipment")
                rType = ResourceType.Equipment;
            else
                throw new ArgumentOutOfRangeException(nameof(type));

            var loginUser = new ClaimsApplicationUser(User);
            var resource = await _resourceService.GetAsync(url, rType);

            var authorizationResult = await _authorizationService.AuthorizeAsync(User, resource, "ResourceReserve");
            if (authorizationResult.Succeeded)
            {
                var inputStartDate = Convert.ToDateTime(input.startDate + ' ' + input.startTime);
                var inputEndDate = Convert.ToDateTime(input.endDate + ' ' + input.endTime);

                var vEvent = new CalendarEvent()
                {
                    // include the time no matter what - it will be ignore if isAllDay=true
                    Start = new CalDateTime(inputStartDate, "America/Chicago"),
                    End = new CalDateTime(inputEndDate, "America/Chicago"),
                    IsAllDay = input.isAllDay,
                    Summary = input.title,
                    Description = input.descr
                };

                // check for recurrence
                var frequency = input.frequency.ToLower();
                if (frequency != "never")
                {
                    RecurrencePattern rrule;

                    switch (frequency)
                    {
                        case "weekly":
                            rrule = new RecurrencePattern(FrequencyType.Weekly, input.recurInterval)
                            {
                                ByDay = DaysToICalDays(input.byDay)
                            };

                            break;
                        case "monthly":
                            rrule = new RecurrencePattern(FrequencyType.Monthly, input.recurInterval);

                            if (input.monthlyRepeatOn == "day")
                            {
                                rrule.ByMonthDay = new List<int>() { input.recurMonthDay };
                            }
                            else if (input.monthlyRepeatOn == "weekday")
                            {
                                rrule.BySetPosition = new List<int>() { input.recurWeekDayOffset };
                                rrule.ByDay = new List<WeekDay>() { new WeekDay(input.recurWeekDay) };
                            }
                            else
                                throw new ArgumentException("monthlyRepeatOn must specify day or weekday");
                            break;
                        default:
                            throw new ArgumentOutOfRangeException($"Recurrence frequency {frequency} not supported.");
                    }

                    //End
                    if (input.repeatEnd == "after")
                    {
                        rrule.Count = input.endAfter;
                    }
                    else if (input.repeatEnd == "on")
                    {
                        if (DateTime.TryParse(input.repeatUntil, out DateTime until))
                            rrule.Until = until;
                        else
                            throw new ArgumentException("repeatUntil is not a vaild date");
                    }

                    // if "never ends", then just don't set anything

                    vEvent.RecurrenceRules = new List<RecurrencePattern>() { rrule };
                }


                var request = new ReservationRequest(vEvent, resource, loginUser);
                await _reservationProcessor.Process(request);

                return NoContent();
            }
            else
            {
                return new ForbidResult();
            }

        }

        /// <summary>
        /// Converts a string of UW day(s) to a list of ICal DayOfWeek
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        private List<WeekDay> UWDaysToICalDays(string days)
        {
            var arDays = days.Split();
            var lstDays = new List<WeekDay>();
            foreach (var day in arDays)
            {
                lstDays.Add(new WeekDay(UWDayToICalDay(day)));
            }

            return lstDays;
        }

        /// <summary>
        /// Converts an array of day(s) to a list of ICal DayOfWeek
        /// </summary>
        /// <param name="days">in the format MO, TU, WE, etc</param>
        private List<WeekDay> DaysToICalDays(string[] days)
        {
            var lstDays = new List<WeekDay>();
            foreach (var day in days)
            {
                lstDays.Add(new WeekDay(day));
            }

            return lstDays;
        }

        /// <summary>
        /// Converts a UW day into a <see cref="DayOfWeek"/>
        /// </summary>
        private DayOfWeek UWDayToICalDay(string day)
        {
            switch (day.ToUpper())
            {
                case "M":
                    return DayOfWeek.Monday;
                case "T":
                    return DayOfWeek.Tuesday;
                case "W":
                    return DayOfWeek.Wednesday;
                case "R":
                    return DayOfWeek.Thursday;
                case "F":
                    return DayOfWeek.Friday;
                case "S":
                    return DayOfWeek.Saturday;
                case "N":
                    return DayOfWeek.Sunday;
                default:
                    throw new ArgumentOutOfRangeException(nameof(day));
            }
        }
    }
}