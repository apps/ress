﻿using AAE.RESS.Application;
using AAE.RESS.Interfaces;
using AAE.RESS.Web.ViewModels;
using Ical.Net.CalendarComponents;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Controllers
{
    public class CalendarController : Controller
    {
        private readonly IResourceService _resourceService;
        private readonly ICalendarRepository _calendarRepository;
        public CalendarController(IResourceService resourceService, ICalendarRepository calendarRepository)
        {
            _resourceService = resourceService ?? throw new ArgumentNullException(nameof(resourceService));
            _calendarRepository = calendarRepository ?? throw new ArgumentNullException(nameof(calendarRepository));
        }

        [HttpGet("{type:regex(rooms|equipment)}/{url}/calendar")]
        public async Task<IActionResult> Index(string type, string url)
        {
            ResourceType rType;
            if (type == "rooms")
                rType = ResourceType.Room;
            else if (type == "equipment")
                rType = ResourceType.Equipment;
            else
                throw new ArgumentOutOfRangeException(nameof(type));


            var resource = await _resourceService.GetAsync(url, rType);

            var vm = new CalendarViewModel(resource);

            return View(vm);
        }

        [HttpGet("{type:regex(rooms|equipment)}/{url}/calendar/ics")]
        public async Task<IActionResult> Ics(string type, string url, [FromQuery] DateTimeOffset start, [FromQuery] DateTimeOffset end)
        {
            ResourceType rType;
            if (type == "rooms")
                rType = ResourceType.Room;
            else if (type == "equipment")
                rType = ResourceType.Equipment;
            else
                throw new ArgumentOutOfRangeException(nameof(type));

            var vevents = await _calendarRepository.GetEventsAsync(rType, url);

            var non_recurring_vevents = vevents.Where(ev => ev.DtStart.AsDateTimeOffset <= end
                                        && ev.DtEnd.AsDateTimeOffset >= start
                                        && ev.RecurrenceRules.Count == 0
                                        ).ToList();

            // this will be used to search for "exceptions" to recurring events
            var recurring_exceptions = non_recurring_vevents.Where(ev => ev.RecurrenceId != null)
                                            .ToList();

            var occurrences = vevents
                        .Where(ev => ev.RecurrenceRules.Count > 0)
                        .AsParallel()
                        .SelectMany(e => e.GetOccurrences(start.DateTime, end.DateTime))
                        .Distinct()
                        .ToList();

            IList<FullCalendarEvent> out_events = new List<FullCalendarEvent>();
            FullCalendarEvent out_event;

            // loop through non-recurring events
            foreach (var vevent in non_recurring_vevents)
            {
                out_event = new FullCalendarEvent()
                {
                    id = vevent.Uid,
                    allDay = vevent.IsAllDay,
                    start = vevent.DtStart.AsDateTimeOffset,
                    end = vevent.DtEnd.AsDateTimeOffset,
                    // title = vevent.Properties.Get<string>("X-MICROSOFT-CDO-BUSYSTATUS")
                    title = vevent.Summary

                };

                out_events.Add(out_event);
            }

            // loop through recurring event occurences
            foreach (var occurrence in occurrences)
            {
                var vevent = (CalendarEvent)occurrence.Source;

                // are there any exceptions for this particular occurrence?
                // criteria is UID match and DtStart is the same day (not necessarily the time)
                // https://www.kanzaki.com/docs/ical/recurrenceId.html

                var vevent_occurence_exception = recurring_exceptions.Where(ev => ev.Uid == vevent.Uid &&
                                                                             ev.DtStart.Date == occurrence.Period.StartTime.Date)
                                                                             .FirstOrDefault();

                if (vevent_occurence_exception == null)
                {
                    out_event = new FullCalendarEvent()
                    {
                        id = vevent.Uid,
                        allDay = vevent.IsAllDay,
                        start = occurrence.Period.StartTime.AsDateTimeOffset,
                        end = occurrence.Period.EndTime.AsDateTimeOffset,
                        title = vevent.Summary
                    };

                    out_events.Add(out_event);
                }

            }

            return Json(out_events);
        }

    }
}