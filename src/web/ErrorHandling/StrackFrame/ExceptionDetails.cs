﻿using System;
using System.Collections.Generic;
using AAE.Helpers.StackTrace;

namespace AAE.Web.ErrorHandling
{
    /// <summary>
    /// Contains details for individual exception messages.
    /// </summary>
    public class ExceptionDetails
    {      /// <summary>
           /// An individual exception
           /// </summary>
        public Exception Error { get; set; }

        /// <summary>
        /// The generated stack frames
        /// </summary>
        public IEnumerable<StackFrameSourceCodeInfo> StackFrames { get; set; }

        /// <summary>
        /// Gets or sets the summary message.
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
