﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Primitives;

namespace AAE.Web.ErrorHandling
{
    /// <summary>
    /// Holds data to be displayed in the error model
    /// </summary>
    public class ErrorEmailModel
    {

        /// <summary>
        /// Detailed information about each exception in the stack.
        /// </summary>
        public IEnumerable<ExceptionDetails> ErrorDetails { get; set; }

        /// <summary>
        /// Parsed query data.
        /// </summary>
        public IQueryCollection Query { get; set; }

        /// <summary>
        /// Request cookies.
        /// </summary>
        public IRequestCookieCollection Cookies { get; set; }

        /// <summary>
        /// Request headers.
        /// </summary>
        public IDictionary<string, StringValues> Headers { get; set; }

        /// <summary>
        /// Request route values.
        /// </summary>
        public RouteValueDictionary RouteValues { get; set; }

        /// <summary>
        /// Request endpoint.
        /// </summary>
        public EndpointModel Endpoint { get; set; }

        /// <summary>
        /// Requesting IP address
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// User ID of logged user
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Original requesting URL
        /// </summary>
        public Uri Uri { get; set; }

        /// <summary>
        /// Status code for the error
        /// </summary>
        public int StatusCode { get; set; } = StatusCodes.Status500InternalServerError;
    }
}
