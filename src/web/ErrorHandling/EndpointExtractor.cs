﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Routing;

namespace AAE.Web.ErrorHandling
{
    /// <summary>
    /// Extracts endpoint information from the HttpContext
    /// </summary>
    public static class EndpointExtractor
    {
        public static EndpointModel Extract(HttpContext context)
        {
            var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;

            EndpointModel endpointModel = null;
            if (endpoint != null)
            {
                endpointModel = new EndpointModel();
                endpointModel.DisplayName = endpoint.DisplayName;

                if (endpoint is RouteEndpoint routeEndpoint)
                {
                    endpointModel.RoutePattern = routeEndpoint.RoutePattern.RawText;
                    endpointModel.Order = routeEndpoint.Order;

                    var httpMethods = endpoint.Metadata.GetMetadata<IHttpMethodMetadata>()?.HttpMethods;
                    if (httpMethods != null)
                    {
                        endpointModel.HttpMethods = string.Join(", ", httpMethods);
                    }
                }
            }

            return endpointModel;
        }
    }
}
