﻿using AAE.RESS.Application.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;

namespace AAE.Web.ErrorHandling
{
    public class ExceptionEmailerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IEmailService _emailService;
        private readonly IHostEnvironment _hostEnvironment;

        public ExceptionEmailerMiddleware(RequestDelegate next, IEmailService emailService, IHostEnvironment hostEnvironment)
        {
            _next = next;
            _emailService = emailService;
            _hostEnvironment = hostEnvironment;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                // follows the logic of .net core DeveloperExceptionPageMiddleware
                // https://github.com/dotnet/aspnetcore/blob/dcd32c0d14e1745eebf737bf9f948de309e02027/src/Middleware/Diagnostics/src/DeveloperExceptionPage/DeveloperExceptionPageMiddleware.cs#L236

                var exceptionDetailsProvider = new ExceptionDetailsProvider(_hostEnvironment);

                // get routing data
                var endpointModel = EndpointExtractor.Extract(context);

                var request = context.Request;

                var model = new ErrorEmailModel()
                {
                    Query = request.Query,
                    ErrorDetails = exceptionDetailsProvider.GetDetails(ex),
                    Cookies = request.Cookies,
                    Headers = request.Headers,
                    RouteValues = request.RouteValues,
                    Endpoint = endpointModel,
                    IpAddress = context.Connection.RemoteIpAddress.ToString(),
                    UserId = "Unknown",
                    Uri = new Uri($"{context.Request.Scheme}://{context.Request.Host}{context.Request.Path}")
                };

                //string uaString = context.Request.Headers["User-Agent"].ToString();
                if (context.User.Identity.IsAuthenticated)
                {
                    model.UserId = context.User.Identity.Name;
                }

                var emailBody = ErrorEmailCreator.CreateExceptionEmail(model);

                await _emailService.SendAdminAsync($"Server Error: {model.Uri}", emailBody);

                throw new InvalidOperationException($"Recorded By Middleware: {ex.Message}");

            }
        }
    }

    public static class ExceptionEmailerMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionEmailer(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionEmailerMiddleware>();
        }
    }
}
