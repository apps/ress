﻿namespace AAE.Web.ErrorHandling
{
    public class ErrorModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        public int StatusCode { get; set; }
        public string StatusMessage
        {
            get
            {
                switch (StatusCode)
                {
                    case 404:
                        return "The page you are looking for was not found.";
                    case 500:
                        return "This page generated a server error.";
                    case 403:
                        return "You are not allowed to access this page.";
                    case 401:
                        return "You are not authorized to access this page.";
                    case 400:
                        return "A bad request was made when trying to access this page.";
                    default:
                        return "An error occurred trying to view this page.";

                }

            }
        }
    }
}
