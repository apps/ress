﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using System;

namespace AAE.RESS.Web.Identity
{
    public static class IApplicationBuilderExtensions
    {
        /// <summary>
        /// Enables use of the RESS user identity
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseAppIdentity(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.UseMiddleware<RESSIdentityMiddleware>();
        }

        /// <summary>
        /// Enables RESS identity with the given options
        /// </summary>
        public static IApplicationBuilder UseRESSIdentity(this IApplicationBuilder app, RESSUserOptions options)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            return app.UseMiddleware<RESSIdentityMiddleware>(Options.Create(options));
        }
    }
}
