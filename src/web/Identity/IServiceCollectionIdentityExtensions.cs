﻿using AAE.RESS.Infrastructure.Data.Repositories;
using AAE.RESS.Infrastructure.Identity;
using AAE.RESS.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Identity
{
    public  static class IServiceCollectionIdentityExtensions
    {
        public static IServiceCollection AddAppIdentity(this IServiceCollection services)
        {
            services.AddTransient<IRESSUserManager, RESSUserManager>();
            services.AddTransient<IRESSUserRepository, RESSUserRepository>();

            // gives access to IHttpContextAccessor
            services.AddHttpContextAccessor();

            return services;
        }
    }
}
