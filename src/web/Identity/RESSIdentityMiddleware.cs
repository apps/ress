﻿using AAE.RESS.Interfaces;
using AAE.RESS.Security.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Identity
{
    public class RESSIdentityMiddleware
    {
        private readonly RESSUserOptions _options;
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        /// <summary>
        /// Creates a new instance of the RESSIdentityMiddleware.
        /// </summary>
        /// <param name="next">The next middleware in the pipeline.</param>
        /// <param name="options">The configuration options.</param>
        /// <param name="loggerFactory">An <see cref="ILoggerFactory"/> instance used to create loggers.</param>
        public RESSIdentityMiddleware(RequestDelegate next, IOptions<RESSUserOptions> options, ILoggerFactory loggerFactory)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (loggerFactory == null)
            {
                throw new ArgumentNullException(nameof(loggerFactory));
            }


            _next = next ?? throw new ArgumentNullException(nameof(next));
            _options = options.Value;
            _logger = loggerFactory.CreateLogger<RESSIdentityMiddleware>();
        }


        /// <summary>
        /// Processes a request to either create a RESS user or update it with the current Authentication Identity
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, IRESSUserManager userManager)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                // registers (and/or gets) the app user
                var user_id = await userManager.RegisterUserInAppAsync(new RESSUser(context.User));

                // create the new CC Log identity to add to the existing ClaimsPrincipal
                var user = new RESSUser(context.User, user_id);


                context.User.AddIdentity(user.ToClaimsIdentity());
            }

            

            await _next(context);
        }
    }
}
