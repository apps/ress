﻿using AAE.RESS.Application;
using AAE.RESS.Interfaces;
using System.Collections.Generic;

namespace AAE.RESS.Web.ViewModels
{
    public class TypeViewModel
    {

        public TypeViewModel()
        {
            Resources = new List<IResourceListing>();
        }

        public ResourceType ResourceType { get; set; }

        public string ResourceTypeText { get; set; }

        public IList<IResourceListing> Resources { get; set; }
    }
}
