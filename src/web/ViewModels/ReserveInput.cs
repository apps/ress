﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.ViewModels
{
    public class ReserveInput
    {
        public string title { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public bool isAllDay { get; set; }
        public string descr { get; set; }

        // never, weekly, monthly
        public string frequency { get; set; }

        public int recurInterval { get; set; }

        // "day" for repeating a numbered day of the month
        // "weekday" for "1st Monday of the month"
        public string monthlyRepeatOn { get; set; }

        // Monthly by date "3rd of March"
        public int recurMonthDay { get; set; }

        // monthly by day - 1st (1), 2nd (2), Last (-1), etc
        public int recurWeekDayOffset { get; set; }

        // MO, TU, WE, etc - ignored if monthlyRepeatOn != 'weekday'
        public string recurWeekDay { get; set; }

        // array of 2-letter day indicators ['MO', 'TU'] - for days in the weekly set
        public string[] byDay { get; set; }

        // date for "end by"
        public string repeatUntil { get; set; }

        // "never" for never ending
        // "after for after X occurrences
        // "on" for on DATE (repeatUntil)
        public string repeatEnd { get; set; }

        // end after X occurrances
        public int endAfter { get; set; }

    }
}
