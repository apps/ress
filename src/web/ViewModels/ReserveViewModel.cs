﻿using AAE.RESS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.ViewModels
{
    public class ReserveViewModel
    {
        public ReserveViewModel(IResource resource)
        {
            Resource = resource;
        }
        public IResource Resource { get; set; }
    }
}
