﻿using AAE.RESS.Application.ReadModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.ViewModels
{
    public class InfoRoomViewModel
    {
        public Room Room { get; set; }
    }
}
