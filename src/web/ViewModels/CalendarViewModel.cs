﻿using AAE.RESS.Interfaces;

namespace AAE.RESS.Web.ViewModels
{
    public class CalendarViewModel
    {
        public CalendarViewModel(IResource resource)
        {
            Resource = resource;
        }
        public IResource Resource { get; set; }
    }
}
