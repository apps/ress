﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.ViewModels
{
    /// <summary>
    /// Event for FullCalendar
    /// </summary>
    public class FullCalendarEvent
    {
        public string id { get; set; }
        public bool allDay { get; set; }
        public DateTimeOffset start { get; set; }
        public DateTimeOffset end { get; set; }
        public string title { get; set; }
    }
}
