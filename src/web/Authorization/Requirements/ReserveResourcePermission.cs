﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Authorization.Requirements
{
    /// <summary>
    /// Requirement for permission to reserve a resource.  Also checks if a resource CAN be reserved
    /// </summary>
    public class ReserveResourcePermission : IAuthorizationRequirement
    {
    }
}
