﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Authorization
{
    /// <summary>
    /// Config object holding values for app groups from appsettings
    /// </summary>
    public class AppGroupConfig
    {
        public string SysAdmin { get; set; }
        public string ResourceEditors { get; set; }
    }
}
