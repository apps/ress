﻿using AAE.RESS.Web.Authorization.Handlers;
using AAE.RESS.Web.Authorization.Requirements;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Authorization
{
    public static class AuthorizationExtensions
    {
        /// <summary>
        /// Adds authorization services to the specified <see cref="IServiceCollection" />. 
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection" /> to add services to.</param>
        /// <param name="configure">An action delegate to configure the provided <see cref="AuthorizationOptions"/>.</param>
        /// <returns>The <see cref="IServiceCollection"/> so that additional calls can be chained.</returns>
        public static IServiceCollection AddPermissionsAuthorization(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ResourceReserve", policy =>
                {
                    policy.Requirements.Add(new ReserveResourcePermission());
                });
            });


            services.AddSingleton<IAuthorizationHandler, ReserveResourceHandler>();

            return services;
        }
    }
}
