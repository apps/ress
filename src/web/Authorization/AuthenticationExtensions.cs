﻿using AAE.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using UW.AspNetCore.Authentication;

namespace AAE.RESS.Web.Authorization
{
    public static class AuthenticationExtensions
    {
        /// <summary>
        /// Sets up dev authentication
        /// </summary>
        /// <param name="username">Username of user to impersonate - valid values: ewdieckman, ebbesmeyer</param>
        /// <returns></returns>
        public static IServiceCollection AddAppDevAuthentication(this IServiceCollection services, string username)
        {
            // setup a dictionary with the various users setup

            var auth_dict = new Dictionary<string, IList<Claim>>();

            // ewdieckman - SysAdmin
            auth_dict.Add("ewdieckman", new List<Claim>() {
                                new Claim(StandardClaimTypes.Name, "ewdieckman"),
                                new Claim(StandardClaimTypes.GivenName, "Eric"),
                                new Claim(StandardClaimTypes.Surname, "Dieckman"),
                                new Claim(StandardClaimTypes.PPID, "UW106Z480"),
                                new Claim(StandardClaimTypes.Email, "eric.dieckman@wisc.edu"),
                                new Claim(StandardClaimTypes.Group, "uw:domain:aae.wisc.edu:it:sysadmin"),
                                new Claim(StandardClaimTypes.Group, "uw:domain:aae.wisc.edu:apps:ress:sysadmins")
            });

            // ebbesmeyer - users that are resource editors
            auth_dict.Add("ebbesmeyer", new List<Claim>() {
                                new Claim(StandardClaimTypes.Name, "ebbesmeyer"),
                                new Claim(StandardClaimTypes.GivenName, "Erin"),
                                new Claim(StandardClaimTypes.Surname, "Ebbesmeyer"),
                                new Claim(StandardClaimTypes.PPID, "UW834D494"),
                                new Claim(StandardClaimTypes.Email, "erin.ebbesmeyer@wisc.edu"),
                                new Claim(StandardClaimTypes.Group, "uw:domain:aae.wisc.edu:apps:ress:resource-editors")
            });

            // lvdavis - staff member than can automatically reserve
            auth_dict.Add("lvdavis", new List<Claim>() {
                                new Claim(StandardClaimTypes.Name, "lvdavis"),
                                new Claim(StandardClaimTypes.GivenName, "Linda"),
                                new Claim(StandardClaimTypes.Surname, "Davis"),
                                new Claim(StandardClaimTypes.PPID, "UW117Y494"),
                                new Claim(StandardClaimTypes.Email, "linda.davis@wisc.edu"),
                                new Claim(StandardClaimTypes.Group, "uw:domain:aae.wisc.edu:administrativestaff")
            });

            // sjohnston3 - general users
            auth_dict.Add("sjohnston3", new List<Claim>() {
                                new Claim(StandardClaimTypes.Name, "sjohnston3"),
                                new Claim(StandardClaimTypes.GivenName, "Sarah"),
                                new Claim(StandardClaimTypes.Surname, "Johnston"),
                                new Claim(StandardClaimTypes.PPID, "UW525C550"),
                                new Claim(StandardClaimTypes.Email, "sarah.johnston@wisc.edu"),
            });

            // fbares - FERPA student
            auth_dict.Add("fbares", new List<Claim>() {
                                new Claim(StandardClaimTypes.Name, "fbares"),
                                new Claim(StandardClaimTypes.GivenName, "Francois"),
                                new Claim(StandardClaimTypes.Surname, "Bares"),
                                new Claim(StandardClaimTypes.PPID, "UW087X092"),
            });

            // check for the specified user
            if (!auth_dict.TryGetValue(username, out IList<Claim> claims))
                throw new Exception($"{username} is not a valid dev authentication name");

            // now actually put the user in there
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = DevAuthenticationDefaults.AuthenticationScheme;
            })
            .AddDevAuthentication(claims);

            return services;
        }

    }
}
