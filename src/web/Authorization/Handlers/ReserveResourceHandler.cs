﻿using AAE.RESS.Interfaces;
using AAE.RESS.Web.Authorization.Requirements;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Authorization.Handlers
{
    /// <summary>
    /// Handler for <see cref="ReserveResourcePermission"/>
    /// </summary>
    public class ReserveResourceHandler : AuthorizationHandler<ReserveResourcePermission, IResource>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ReserveResourcePermission requirement, IResource resource)
        {

            // succeeds if the resource "CanReserve".  Otherwise - outright fail.  Does not allow requirements to override
            if (resource.CanReserve)
                context.Succeed(requirement);
            else
                context.Fail();
                   
            return Task.CompletedTask;
        }
    }
}
