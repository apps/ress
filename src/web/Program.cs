using AAE.Amazon.Util;
using AAE.Caching;
using AAE.RESS;
using AAE.RESS.Application.Images;
using AAE.RESS.Application.Interfaces;
using AAE.RESS.Application.Reserve;
using AAE.RESS.Config;
using AAE.RESS.Infrastructure;
using AAE.RESS.Infrastructure.Data.Factories;
using AAE.RESS.Infrastructure.Data.Queries;
using AAE.RESS.Infrastructure.Data.Repositories;
using AAE.RESS.Interfaces;
using AAE.RESS.Settings;
using AAE.RESS.Web.Authorization;
using AAE.RESS.Web.Helpers;
using AAE.RESS.Web.Identity;
using AAE.RESS.Web.Security;
using AAE.Web.ErrorHandling;
using Amazon.Runtime;
using Amazon.S3;
using Azure.Identity;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json.Serialization;
using System.Security.Cryptography.X509Certificates;
using UW.AspNetCore.Authentication;
using UW.Shibboleth;

var builder = WebApplication.CreateBuilder(args);

ConfigureAzureKeyVault(builder);

ConfigureAuthentication(builder, "ewdieckman");

ConfigureViews(builder);


builder.Services.AddInMemoryCaching();

builder.Services.AddHttpContextAccessor();
builder.Services.AddHttpClient();

// used by tag helpers for resolving virtual paths:   ~/file.jpg
builder.Services
    .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
    .AddScoped(x => x
        .GetRequiredService<IUrlHelperFactory>()
        .GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext));

// ############# AUTHORIZATION ################
var appgroups_config = new AppGroupConfig();
builder.Configuration.GetSection("Groups").Bind(appgroups_config);
var app_groups = new AppGroups(
    sysadmin: appgroups_config.SysAdmin,
    resource_editors: appgroups_config.ResourceEditors);
builder.Services.AddSingleton(app_groups);
builder.Services.AddPermissionsAuthorization();

builder.Services.AddAppIdentity();

// ############# CONNECTION FACTORIES ################
builder.Services.AddTransient<IDbConnectionFactoryRESS, DbConnectionFactoryRESS>(c => new DbConnectionFactoryRESS(builder.Configuration.GetConnectionString("ress")));

builder.Services.Configure<MailSettings>(builder.Configuration.GetSection("EmailSettings"));
builder.Services.AddTransient<IEmailService, EmailService>();

// ############# SERVICES ################
builder.Services.AddScoped<IResourceService, ResourceService>();

// ############# QUERIES ################
builder.Services.AddScoped<IRoomQueries, RoomQueries>();
builder.Services.AddScoped<IEquipmentQueries, EquipmentQueries>();

builder.Services.AddScoped<IReservationProcessor, ReservationProcessor>();

builder.Services.TryAddTransient(typeof(CalendarRepository));
builder.Services.AddTransient<ICalendarRepository, CalendarCachedRepository<CalendarRepository>>();

// ############# AWS ################
var awsOptions = builder.Configuration.GetAWSOptions();
awsOptions.Credentials = new BasicAWSCredentials(builder.Configuration["AWS:AccessKeyId"], builder.Configuration["AWS:SecretAccessKey"]);
builder.Services.AddDefaultAWSOptions(awsOptions);
builder.Services.AddAWSService<IAmazonS3>(awsOptions);
builder.Services.AddSingleton(builder.Configuration.GetSection("AmazonS3").Get<S3Config>());

// #########################   UTILITIES ##############################
// add the generated width sizes for uploaded resource images
// using the legacy widths of 2048, 600, 320 - should replicate responsive widths at some point
// https://wiki.aae.wisc.edu/en/app-development/web/responsive
builder.Services.AddSingleton<IResourceImageGeneratedWidthCollection, ResourceImageGeneratedWidthCollection>(coll => new ResourceImageGeneratedWidthCollection(
    new List<KeyValuePair<ResponsiveImageNames, short>>(){
                    new KeyValuePair<ResponsiveImageNames, short>(ResponsiveImageNames.FullHd,2048),
                    new KeyValuePair<ResponsiveImageNames, short>(ResponsiveImageNames.Desktop,600),
                    new KeyValuePair<ResponsiveImageNames, short>(ResponsiveImageNames.Thumbnail,320),
    }));
builder.Services.AddSingleton<IResourceImageUtility, ResourceImageUtility>();
builder.Services.AddAmazonUtilities();

var app = builder.Build();

app.UseStaticFiles();

if (!app.Environment.IsDevelopment())
{
    app.UseHsts();
    app.UseExceptionHandler("/error");
    app.UseStatusCodePagesWithReExecute("/error/{0}");
    app.UseExceptionEmailer();
}

// AFTER UseStaticFiles, UseExceptionHandler or UseStatusCodePagesWithReExecute but BEFORE UseAuthentication and UseAuthorization
app.UseRouting();

app.UseAuthentication();
app.UseHttpsRedirection();
app.UseResponseCompression();

app.UseAppIdentity();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
});

if (app.Environment.IsDevelopment())
{
    app.UseSpa(spa =>
    {
        spa.Options.SourcePath = "ClientApp";
        spa.Options.DevServerPort = Convert.ToInt32(builder.Configuration["Vite:Server:Port"]);
        spa.UseProxyToSpaDevelopmentServer(() =>
        {
            return Task.FromResult(new UriBuilder("http", "localhost", spa.Options.DevServerPort).Uri);
        });
    });
}

app.Run();


// Configures the Azure Key Vault for this app
void ConfigureAzureKeyVault(WebApplicationBuilder builder)
{
    if (!builder.Environment.IsDevelopment())
    {
        using var x509Store = new X509Store(StoreLocation.LocalMachine);

        x509Store.Open(OpenFlags.ReadOnly);

        var certs = x509Store.Certificates;
        var distinguishedName = new X500DistinguishedName(builder.Configuration["Azure:KeyVault:SubjectDistinguishedName"]);
        var certFound = certs
            .Find(
                X509FindType.FindBySubjectDistinguishedName,
                distinguishedName.Name,
                validOnly: false)
            .OfType<X509Certificate2>();

        if (!certFound.Any())
            throw new Exception("Unable to find the certificate to authenticate and access Azure Key Vault");

        builder.Configuration.AddAzureKeyVault(
            new Uri(builder.Configuration["Azure:KeyVault:Endpoint"]),
            new ClientCertificateCredential(
                builder.Configuration["Azure:AD:DirectoryId"],
                builder.Configuration["Azure:AD:ApplicationId"],
                certFound.Single()));
    }
}

// Configures authentication for the app
void ConfigureAuthentication(WebApplicationBuilder builder, string devAuthenticationUsername)
{
    if (!builder.Environment.IsDevelopment())
    {
        //authentication with Shibboleth
        builder.Services.AddAuthentication(options =>
        {
            options.DefaultScheme = ShibbolethAuthenticationDefaults.AuthenticationScheme;
        }).AddUWShibboleth();
    }
    else
    {
        //authenticate with local devauth
        builder.Services.AddAppDevAuthentication(devAuthenticationUsername);
    }
}



// Add controllers and views with the app config filter.  Sets defaults for routing options
void ConfigureViews(WebApplicationBuilder builder)
{
    builder.Services.AddControllersWithViews(options =>
    {
        options.Filters.Add(new IdentityViewBagActionFilter());
        options.Filters.Add(new AppConfigActionFilter(builder.Configuration.GetSection("AppConfig")));
    })
            .AddNewtonsoftJson(options =>
            {
                //keeps the JSON object's name the same as the .NET class property (keeps CAPS, etc)
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

    builder.Services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
    builder.Services.Configure<RouteOptions>(options => options.AppendTrailingSlash = true);

    builder.Services.AddResponseCompression(options => options.EnableForHttps = true);

}

