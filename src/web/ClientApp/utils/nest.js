﻿import _ from 'lodash';

/**
 * A multi-level groupBy for arrays inspired by D3's nest operator.
 * https://gist.github.com/joyrexus/9837596
 * @param {Array} seq Array of objects with properties
 * @param {Array} keys Array of string values of properties to group by, in order
 */
export function nest(seq, keys) {
    if (!keys.length)
        return seq;
    var first = keys[0];
    var rest = keys.slice(1);
    return _.mapValues(_.groupBy(seq, first), function (value) {
        return nest(value, rest)
    });
}