module.exports = {
    root: true,
    env: {
        es2021: true,
        node: true      // needed for globals like __dirname
    },
    extends: [
        'plugin:vue/vue3-essential',
        'eslint:recommended'
    ],
    ignorePatterns: ['.eslintrc.js', 'vite.config.js', '_uw-style.min.js'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
    }
}
