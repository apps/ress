﻿import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { faCog, faSpinner } from '@fortawesome/pro-solid-svg-icons'
import { faCalendar } from '@fortawesome/pro-regular-svg-icons'

library.add(faCog, faSpinner, faCalendar);

dom.watch();

export default FontAwesomeIcon;
