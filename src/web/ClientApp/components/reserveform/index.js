﻿import { createApp } from 'vue/dist/vue.esm-bundler';
import FormDatePicker from './components/formdatepicker.vue'
import TimePicker from './components/timepicker.vue'
import WeekDayPicker from './components/weekdaypicker.vue'
import DayPicker from './components/daypicker.vue'
import { ToggleButton } from '@/components/toggle-button'
import axios from 'axios'
import VueNumberInput from '@chenfengyuan/vue-number-input';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
dayjs.extend(customParseFormat);

import './scss/reserveform.scss'


const $app = document.getElementById('js-vue-app');
const { dataset } = $app;

var app = createApp({
    components: {
        FormDatePicker,
        ToggleButton,
        TimePicker,
        WeekDayPicker,
        DayPicker,
        VueNumberInput
    },
    data() {
        const now = dayjs();
        return {
            endpoint: dataset.endpoint,
            successurl: dataset.successurl,
            uniqueid: dataset.uniqueid,
            reserveRequest: {
                title: '',
                startDate: now.format('M/D/YYYY'),                  // string date in M/d/yyyy format
                endDate: now.format('M/D/YYYY'),                    // string date in M/d/yyyy format
                startTime: now.add(1,'hour').format('H') + ':00',  // string time in h:mm format
                endTime: now.add(1, 'hour').format('H') + ':00',   // string time in h:mm format                    
                isAllDay: false,                                    // true/false
                descr: '',
                frequency: 'never',                                 // never, weekly, monthly
                endAfter: 1,                                        // int
                recurInterval: 1,                                   // int
                monthlyRepeatOn: 'day',                             // day, weekday
                recurMonthDay: parseInt(now.format('D')),           // int
                recurWeekDay: this.toRRuleWeekday(now),             // MO, TU, WE, etc - ignored if monthlyRepeatOn != 'weekday'
                recurWeekDayOffset: 1,                              // 1-4, -1 for last
                repeatEnd: 'never',                                 // never, after, on
                repeatUntil: '',                                    // string date in M/d/yyyy format - ignored if repeatEnd != 'on'
                byDay: []                                           // Array of strings MO, TU, WE etc
            },

            isSubmitting: false, 
            error: null,

            errorSubmitting: false,

            validations: {
                title: {
                    required: true
                },
                endDate: {
                    afterStart: true,
                },
                endTime: {
                    afterStart: true
                }
            }

        } 
    },
    computed: {
        frequencyNever() {
            return this.reserveRequest.frequency == 'never';
        },
        frequencyWeekly() {
            return this.reserveRequest.frequency == 'weekly'
        },
        frequencyMonthly() {
            return this.reserveRequest.frequency == 'monthly'
        },
        repeatEndOn() {
            return this.reserveRequest.repeatEnd == 'on'
        },
        repeatEndAfter() {
            return this.reserveRequest.repeatEnd == 'after'
        },
        repeatOnWeekday() {
            return this.reserveRequest.monthlyRepeatOn == 'weekday'
        },
        repeatOnDay() {
            return this.reserveRequest.monthlyRepeatOn == 'day'
        },
        datesValid() {
            return this.validations.endDate.afterStart
                && this.validations.endTime.afterStart
        },
        titleValid() {
            return this.validations.title.required
        },
        startDateNotBeforeEndDate() {
            return this.validations.endDate.afterStart
                && this.validations.endTime.afterStart
        },

    },
    methods: {
        /**
            * Creates a dayjs based on the M/D/YYYY H:mm format
            * @param {String} date
            * @param {String} time
            */
        createDayJs(date, time) {
            return dayjs(date + ' ' + time, 'M/D/YYYY H:mm')
        },

        /**
            * Returns the RRULE WeekDay for the specified dayjs
            * @param {dayjs} dayj
            */
        toRRuleWeekday(dayj) {
            return dayj.format('dddd').substring(0, 2).toUpperCase()
        },

        /**
            * Sets the recurWeekDay and recurWeekDayOffset coming from the event from the weekdaypicker component
            * @param {Object} param0 - Incoming data from weekdaypicker
            * @param {string} param0.offsetvalue - Week day offset  (1st, 2nd, last, etc)
            * @param {string} param0.dayvalue - Day value (MO, TU, FR, etc)
            */
        recurWeekdayInput({ offsetvalue, dayvalue }) {
            this.reserveRequest.recurWeekDay = dayvalue
            this.reserveRequest.recurWeekDayOffset = offsetvalue
        },

        /**
            * Button click for submitting reservation to the server
            * */
        submitReservation() {
            this.isSubmitting = true

            if (this.validateForm()) {
                axios.post(this.endpoint, this.reserveRequest)
                    .then(() => {
                        window.location = this.successurl
                    })
                    .catch((error) => {
                        console.log(error.response)
                        this.error = error
                        this.errorSubmitting = true
                    })
                    .finally(() => {
                        this.isSubmitting = false
                    })
            }
            else {
                this.isSubmitting = false
            }

        },

        /**
            * Event handler for the "close" button on the error alery
            * */
        dismissAlert() {
            this.errorSubmitting = false
        },

        /**
            * Validates the form
            * @returns {Boolean}
            * */
        validateForm() {
            var request = this.reserveRequest

            if (!request.isAllDay) {
                var start = dayjs(request.startDate + ' ' + request.startTime, 'M/D/YYYY H:mm')
                var end = dayjs(request.endDate + ' ' + request.endTime, 'M/D/YYYY H:mm')

                this.validations.endDate.afterStart = end > start
                this.validations.endTime.afterStart = end > start
            }
            else {
                this.validations.endDate.afterStart = true;
                this.validations.endTime.afterStart = true;
            }
            

            this.validations.title.required = request.title.trim().length > 0

            return this.validations.endDate.afterStart
                && this.validations.endTime.afterStart
                && this.validations.title.required
        }
    },
    watch: {
        /**
            * Watching the startDate for use in updating the endDate, recurring Month day, and recurring Weekday
            * @param {string} val - Date in M/D/YYYY format
            */
        'reserveRequest.startDate': function (val) {
            var request = this.reserveRequest
            request.endDate = val
            request.recurMonthDay = parseInt(val.split('/')[1])

            var startMoment = this.createDayJs(request.startDate, request.startTime)
            request.recurWeekDay = this.toRRuleWeekday(startMoment)
        }
            
    }

         
}).mount($app);

export default () => { return app; }
