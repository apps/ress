﻿import { createApp } from 'vue/dist/vue.esm-bundler';

import '@fullcalendar/core/vdom' // solves problem with Vite
import FullCalendar from '@fullcalendar/vue3';
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import Loading from 'vue-loading-overlay';
import { VueModal } from '@aae/vue-modal';
import dayjs from 'dayjs';

import './scss/resourceCalendar.scss'

const $app = document.getElementById('js-vue-app');

var app = createApp({
    components: {
        FullCalendar,
        Loading,
        VueModal
    },
    data() {
        return {
            isCalendarLoading: true,
            selectedEvent: {},
            showEventModal: false,
            calendarOptions: {
                plugins: [dayGridPlugin, timeGridPlugin],
                initialView: 'dayGridMonth',
                headerToolbar: {
                    start: 'prev,next today',
                    center: 'title',
                    end: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                slotMinTime: '06:00:00',
                slotMaxTime: '18:00:00',
                height: 'auto',
                events: $app.dataset.endpoint,
                displayEventEnd: true,
                loading: this.calendarLoading,
                eventClick: this.eventClick
            }
        }
    },
    computed: {
        selectedEventStart() {
            if ('start' in this.selectedEvent) {
                var start = this.selectedEvent.start
                return dayjs(start).format('h:mma');
            }
            else {
                return ''
            }
                
        },
        selectedEventEnd() {
            if ('end' in this.selectedEvent) {
                var end = this.selectedEvent.end
                return dayjs(end).format('h:mma');
            }
            else {
                return ''
            }

        },
        selectedEventIsAllDay() {
            if ('allDay' in this.selectedEvent) {
                return this.selectedEvent.allDay
            }
            else {
                return false
            }
        }
    },
    methods: {
        calendarLoading(bool) {
            this.isCalendarLoading = bool;
        },

        /**
            * Click on the event
            * @param {eventClickInfo} info - object with event click info
            * @param {Object} info.event - The associated Event Object (calendar info).
            * @param {Object} info.el - The HTML element for this event.
            * @param {Object} info.jsEvent - The native JavaScript event with low-level information such as click coordinates.
            * @param {Object} info.view - The current View Object.
            */
        eventClick(info) {
                
            this.selectedEvent = info.event

            this.showEventModal = true;
        },
        onModalClose() {
            this.showEventModal = false;
        }
    },
}).mount($app);

export default () => { return app; }

