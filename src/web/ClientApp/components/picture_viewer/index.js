﻿import { createApp } from 'vue/dist/vue.esm-bundler';

import ModalPictureViewer from './ModalPictureViewer.vue'
import ModalPictureLink from './ModalPictureLink.vue'

var app = createApp({
    components: {
        ModalPictureViewer,
        ModalPictureLink
    },
    data() {
        return {
            modalPictureUrl: '',
            pictureViewerShow: false
        }
    },
    methods: {
        /**
         * Shows the modal
         * */
        show() {
            this.pictureViewerShow = true;
        },

        onModalClose() {
            this.pictureViewerShow = false;
            //this.modalPictureUrl = '';
            
            
        },

        /**
         * Handles the click event from the ModalPictureLink
         * @param {String} url - url of picture to be shown in the modal
         */
        linkClickHandler(url) {
            this.modalPictureUrl = url
            this.show()
        }


    }

}).mount('#js-vue-app');

export default () => { return app; }

