﻿
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';
import generatePages from './utils/generatePages';
import appsettingsDev from "../appsettings.Development.json";
const input = generatePages();

console.log(input)

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './'),
            '~': path.resolve(__dirname, './node_modules/')
        },
    },
    build: {
        outDir: '../wwwroot',
        emptyOutDir: true,
        manifest: true,
        rollupOptions: {
            input
        }
    },
    server: {
        hmr: {
            protocol: 'ws',
            clientPort: appsettingsDev.Vite.Server.Port
        }
    }
})