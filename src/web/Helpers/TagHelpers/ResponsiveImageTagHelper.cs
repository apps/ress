﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using System.IO;

namespace AAE.RESS.Web.TagHelpers
{
    /// <summary>
    /// Creates srcset for supplied image file name
    /// </summary>
    [HtmlTargetElement("img", Attributes = "file-name,path")]
    public class ResponsiveImageTagHelper : TagHelper
    {
        private IUrlHelper _urlHelper;

        public ResponsiveImageTagHelper(IUrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
        }

        public string FileName { get; set; }
        public string Path { get; set; }
        public bool LinkTo { get; set; } = false;
        public string ClickHandler { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var fi = new FileInfo(FileName);

            var srcset = new List<string>();

            if (Path.Substring(Path.Length - 1) == "/")
                Path = Path.Substring(Path.Length - 2);

            srcset.Add($"{_urlHelper.Content(Path)}/{fi.Name.Replace(fi.Extension, "")}-320w{fi.Extension} 320w");
            srcset.Add($"{_urlHelper.Content(Path)}/{fi.Name.Replace(fi.Extension, "")}-600w{fi.Extension} 600w");
            srcset.Add($"{_urlHelper.Content(Path)}/{fi.Name.Replace(fi.Extension, "")}-2048w{fi.Extension} 2048w");

            output.Attributes.Add("src", $"{_urlHelper.Content(Path)}/{fi.Name.Replace(fi.Extension, "")}-320w{fi.Extension}");
            output.Attributes.Add("srcset", string.Join(",",srcset));

            //output.PreElement.SetHtmlContent($"<a href=\"{ _urlHelper.Content(Path)}/{ fi.Name.Replace(fi.Extension, "")}-2048w{ fi.Extension}\" target=\"_blank\" rel=\"noopener noreferrer\">");
            //output.PostElement.SetHtmlContent("</a>");
            string hiResUrl = $"{ _urlHelper.Content(Path)}/{ fi.Name.Replace(fi.Extension, "")}-2048w{ fi.Extension}";
            string clickEventHandler = (!string.IsNullOrEmpty(ClickHandler)) ? $" v-on:link-clicked=\"{ClickHandler}\" " : "";

            output.PreElement.SetHtmlContent($"<modal-picture-link hires=\"{hiResUrl}\" viewurl=\"{hiResUrl}\" {clickEventHandler}>");
            output.PostElement.SetHtmlContent("</modal-picture-link>");

        }
    }
}
