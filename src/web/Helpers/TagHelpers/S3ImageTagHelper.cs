﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using System.IO;

namespace AAE.RESS.Web.TagHelpers
{
    /// <summary>
    /// Creates creates necessary clickable links for S3 images
    /// </summary>
    [HtmlTargetElement("img", Attributes = "url,hi-res-url")]
    public class S3ImageTagHelper : TagHelper
    {
        public string HiResUrl { get; set; }
        public string Url { get; set; }
        public bool LinkTo { get; set; } = false;
        public string ClickHandler { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.Add("src", Url);

            string clickEventHandler = (!string.IsNullOrEmpty(ClickHandler)) ? $" v-on:link-clicked=\"{ClickHandler}\" " : "";

            output.PreElement.SetHtmlContent($"<modal-picture-link hires=\"{HiResUrl}\" viewurl=\"{HiResUrl}\" {clickEventHandler}>");
            output.PostElement.SetHtmlContent("</modal-picture-link>");

        }
    }
}
