﻿using AAE.RESS.Security.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Security.Claims;

namespace AAE.RESS.Web.Helpers
{
    public class IdentityViewBagActionFilter : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Controller is Controller)
            {
                if (context.HttpContext.User.Identity.IsAuthenticated)
                    ((Controller)context.Controller).ViewBag.User = new RESSUser((ClaimsIdentity)context.HttpContext.User.Identity);
            }
                
            base.OnResultExecuting(context);
        }

    }
}
