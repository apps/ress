﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Helpers
{
    /// <summary>
    /// Class to hold app config options from appsettings
    /// </summary>
    public class AppConfig
    {
        public string Branding { get; set; }
    }
}
