﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace AAE.RESS.Web.Helpers
{
    /// <summary>
    /// Action filter to inject app config from appsettings into views
    /// </summary>
    public class AppConfigActionFilter : IAsyncActionFilter
    {
        private AppConfig _options;

        public AppConfigActionFilter(IConfiguration configuration)
        {
            _options = new AppConfig();
            configuration.Bind(_options);
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.Controller is Controller)
                ((Controller)context.Controller).ViewBag.AppConfig = _options;
            await next();
        }
    }
}
