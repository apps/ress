﻿namespace AAE.RESS.Config
{
    /// <summary>
    /// Holds configuration parameters for Amazon S3 use
    /// </summary>
    public class S3Config
    {
        public string RESSBucketName { get; set; } = default;
    }
}
