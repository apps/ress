﻿using AAE.RESS.Interfaces;
using System;

namespace AAE.RESS.Email
{
    /// <summary>
    /// Class to hold app email settings
    /// </summary>
    [Obsolete]
    public class EmailConfig
    {
        public string MailServer { get; set; }
        public int MailPort { get; set; }
        public string SenderName { get; set; }
        public string Sender { get; set; }
        public string SysAdminEmail { get; set; }
    }
}
