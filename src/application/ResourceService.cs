using AAE.RESS.Application;
using AAE.RESS.Application.ReadModels;
using AAE.RESS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AAE.RESS
{
    /// <summary>
    /// Service for interacting with resources
    /// </summary>
    public class ResourceService : IResourceService
    {
        IRoomQueries _roomQueries;
        IEquipmentQueries _equipmentQueries;
        public ResourceService(IRoomQueries roomQueries, IEquipmentQueries equipmentQueries)
        {
            _equipmentQueries = equipmentQueries ?? throw new ArgumentNullException(nameof(equipmentQueries));
            _roomQueries = roomQueries ?? throw new ArgumentNullException(nameof(roomQueries));
        }

        public async Task<IResource> GetAsync(int Id, ResourceType type)
        {
            switch (type)
            {
                case ResourceType.Equipment:
                    return await _equipmentQueries.GetAsync(Id);

                case ResourceType.Room:
                    return await _roomQueries.GetAsync(Id);

                default:
                    throw new NotImplementedException();
            }
        }

        public async Task<IResource> GetAsync(string url, ResourceType type)
        {
            switch (type)
            {
                case ResourceType.Equipment:
                    return await _equipmentQueries.GetAsync(url);

                case ResourceType.Room:
                    return await _roomQueries.GetAsync(url);

                default:
                    throw new NotImplementedException();
            }
        }

        public async Task<IList<IResourceListing>> GetAsync(ResourceType type)
        {
            switch (type)
            {
                case ResourceType.Equipment:
                    var equipment = await _equipmentQueries.GetActiveListingAsync();
                    return new List<IResourceListing>(equipment);

                case ResourceType.Room:
                    var rooms = await _roomQueries.GetActiveListingAsync();
                    return new List<IResourceListing>(rooms);

                default:
                    throw new NotImplementedException();
            }
        }


    }
}
