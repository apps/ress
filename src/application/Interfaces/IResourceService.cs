﻿using AAE.RESS.Application;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAE.RESS.Interfaces
{
    public interface IResourceService
    {
        Task<IResource> GetAsync(int Id, ResourceType type);
        Task<IResource> GetAsync(string url, ResourceType type);
        Task<IList<IResourceListing>> GetAsync(ResourceType type);
    }
}