﻿using AAE.RESS.Security.Identity;
using System.Threading.Tasks;

namespace AAE.RESS.Interfaces
{
    public interface IRESSUserRepository
    {   
        /// <summary>
        /// Adds user to the persistence store
        /// </summary>
        /// <returns>Persistence unique key</returns>
        Task<int> AddAsync(RESSUserInfo user);

        Task<RESSUserInfo> GetAsync(int user_id);
        Task<RESSUserInfo> GetAsync(string username);
        Task<RESSUserInfo> GetAsyncByUniqueID(string unique_id);

        /// <summary>
        /// Update user to the persistence store
        /// </summary>
        Task UpdateAsync(RESSUserInfo user);
    }
}