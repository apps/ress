﻿using System.Data;

namespace AAE.RESS.Interfaces
{
    /// <summary>
    /// Interface for a factory for creating db connections
    /// </summary>
    public interface IDbConnectionFactory
    {
        IDbConnection GetDbConnection();
    }
}
