using AAE.RESS.Application;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Interfaces
{
    public interface IResource
    {
        ResourceType ResourceType { get; set; }
        string DisplayName { get; set; }
        //string ShortName { get; set; }
        string Office365EmailAddress { get; set; }
        string Office365ICSUrl { get; set; }
        int Id { get; set; }
        string Description { get; set; }
        string Url { get; set; }
        bool CanReserve { get; set; }
        IList<IResourceImage> Images { get; set; }
    }
}
