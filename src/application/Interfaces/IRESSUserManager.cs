﻿using AAE.Identity;
using System.Threading.Tasks;

namespace AAE.RESS.Interfaces
{
    public interface IRESSUserManager
    {
        Task<int> RegisterUserInAppAsync(IApplicationUserInfo user);
    }
}