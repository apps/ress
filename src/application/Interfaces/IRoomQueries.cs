﻿using AAE.RESS.Application.ReadModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAE.RESS.Interfaces
{
    public interface IRoomQueries
    {
        /// <summary>
        /// Return the room for the specified primary key
        /// </summary>
        Task<Room> GetAsync(int roomID);

        /// <summary>
        /// Return the room for the specified url
        /// </summary>
        Task<Room> GetAsync(string url);

        /// <summary>
        /// Returns all active rooms
        /// </summary>
        Task<IList<IResourceListing>> GetActiveListingAsync();
    }
}