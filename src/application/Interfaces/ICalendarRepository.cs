using AAE.RESS.Application;
using Ical.Net.CalendarComponents;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAE.RESS.Interfaces
{
    public interface ICalendarRepository
    {
        Task<IList<CalendarEvent>> GetEventsAsync(ResourceType type, string resourceUrl);
    }
}
