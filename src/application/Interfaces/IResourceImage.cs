﻿using AAE.RESS.Application.Images;

namespace AAE.RESS.Interfaces
{
    public interface IResourceImage
    {
        string Filename { get; set; }
        string Subfolder { get; set; }
        string Guid { get; set; }
        short Height { get; set; }
        int Id { get; set; }
        short Width { get; set; }
        ImageUriList Uri { get; set; }
    }
}