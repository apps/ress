﻿using AAE.RESS.Application.ReadModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAE.RESS.Interfaces
{
    public interface IEquipmentQueries
    {
        /// <summary>
        /// Return the equipment for the specified primary key
        /// </summary>
        Task<Equipment> GetAsync(int roomID);

        /// <summary>
        /// Return the equipment for the specified url
        /// </summary>
        Task<Equipment> GetAsync(string url);

        /// <summary>
        /// Returns all active equipment
        /// </summary>
        Task<IList<IResourceListing>> GetActiveListingAsync();
    }
}
