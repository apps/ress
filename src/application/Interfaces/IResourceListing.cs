using AAE.RESS.Application;

namespace AAE.RESS.Interfaces
{
    public interface IResourceListing
    {
        ResourceType ResourceType { get; set; }
        string ResourceSubType { get; set; }
        short ResourceSubTypeSortIndex { get; set; }
        string DisplayName { get; set; }
        int Id { get; set; }
        string Info { get; set; }
        string Url { get; set; }
        bool CanReserve { get; set; }
        IResourceImage CoverImage { get; set; }
    }
}
