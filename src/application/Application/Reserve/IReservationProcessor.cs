﻿using System.Threading.Tasks;

namespace AAE.RESS.Application.Reserve
{
    public interface IReservationProcessor
    {
        Task Process(ReservationRequest reservation);
    }
}