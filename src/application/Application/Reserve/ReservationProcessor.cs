using AAE.RESS.Email;
using Ical.Net;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Hosting;
using System.Text;
using System.Threading.Tasks;
using AAE.RESS.Settings;

namespace AAE.RESS.Application.Reserve
{
    /// <summary>
    /// Processes an incoming reservation
    /// </summary>
    public class ReservationProcessor : IReservationProcessor
    {
        private readonly MailSettings _emailSettings;
        private readonly IHostEnvironment _env;

        public ReservationProcessor(IOptions<MailSettings> emailSettings, IHostEnvironment env)
        {
            _emailSettings = emailSettings.Value;
            _env = env;

        }
        public async Task Process(ReservationRequest reservation)
        {
            var vEvent = reservation.Reservation;
            var organizer = new MailboxAddress(reservation.RequestedBy.DisplayName, reservation.RequestedBy.Email);
            var attendee = new MailboxAddress(reservation.Resource.DisplayName, reservation.Resource.Office365EmailAddress);


            // setup person requesting the resource
            vEvent.Organizer = new Organizer()
            {
                CommonName = organizer.Name,
                Value = new Uri($"mailto:{organizer.Address}")
            };

            // setup the attendees to include the resource
            vEvent.Attendees = new List<Attendee>()
            {
                new Attendee()
                {
                    CommonName = attendee.Name,
                    Rsvp = true,
                    Value = new Uri($"mailto:{attendee.Address}")
                }
            };

            var calendar = new Calendar();
            calendar.Events.Add(vEvent);
            calendar.Method = "REQUEST";

            var serializer = new CalendarSerializer();
            var serializedCalendar = serializer.SerializeToString(calendar);



            // setup outgoing email
            var msg = new MimeMessage();
            msg.From.Add(organizer);

            msg.To.Add(attendee);

            msg.Subject = vEvent.Summary;

            var mixed = new Multipart("mixed");
            mixed.Add(new TextPart("plain")
            {
                Text = vEvent.Description
            });

            var ical = new TextPart("calendar")
            {
                ContentTransferEncoding = ContentEncoding.Base64,
                Text = serializedCalendar.ToString(),
            };
            ical.ContentType.Parameters.Add("method", "REQUEST");
            mixed.Add(ical);

            msg.Body = mixed;

            msg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");

            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                if (_env.IsDevelopment())
                {
                    await client.ConnectAsync(_emailSettings.MailServer, _emailSettings.MailPort,false);
                }
                else
                {
                    await client.ConnectAsync(_emailSettings.MailServer);
                }

                // Note: only needed if the SMTP server requires authentication
                //await client.AuthenticateAsync(_emailSettings.Sender, _emailSettings.Password);

                await client.SendAsync(msg);

                await client.DisconnectAsync(true);
            }

        }
    }
}
