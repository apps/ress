using AAE.Identity;
using AAE.RESS.Interfaces;
using Ical.Net.CalendarComponents;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Application.Reserve
{
    /// <summary>
    /// Used to send out a reservation request to a particular resource
    /// </summary>
    public class ReservationRequest
    {
        public ReservationRequest(CalendarEvent reservation, IResource resource, IApplicationUser requestedBy)
        {
            Reservation = reservation;
            Resource = resource;
            RequestedBy = requestedBy;
        }

        public CalendarEvent Reservation { get; private set; }

        public IResource Resource { get; private set; }

        public IApplicationUser RequestedBy { get; private set; }

    }
}
