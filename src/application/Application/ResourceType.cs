using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Application
{
    /// <summary>
    /// Used to determine a resource type
    /// </summary>
    public enum ResourceType
    {
        Room = 1,
        Equipment = 2
    }
}
