﻿using System;

namespace AAE.RESS.Application.Images
{
    public interface IResourceImageUtility
    {
        IResourceImageGeneratedWidthCollection ResourceImageGeneratedWidths { get; }

        /// <summary>
        /// Generates the filename for image at the specified size:  originalfilename-640w.jpg
        /// </summary>
        string GenerateFilenameWithSize(string originalFilename, short width);

        /// <summary>
        /// Generates a uri list of all the size variants of the headshot
        /// </summary>
        /// <param name="filename">Original filename without path (local or http)</param>
        /// <param name="subfolder">Subfolder, if applicable</param>/// 
        ImageUriList GenerateUriList(string filename, string subfolder = "");

        /// <summary>
        /// Generates the Uri to the original image location
        /// </summary>
        /// <param name="filename">Filename with extension</param>
        /// <param name="subfolder">Subfolder, if applicable</param>
        Uri GetUri(string filename, string subfolder = "");
    }
}