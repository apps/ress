﻿using System;

namespace AAE.RESS.Application.Images
{
    /// <summary>
    /// Contains the list of Uris for each size
    /// </summary>
    /// <seealso cref="AAE.RESS.Application.Images.ResponsiveImageNames"/>
    public class ImageUriList
    {
        // these should match AAE.RESS.Application.Images.ResponsiveImageNames
        public Uri FullHd { get; set; } = null;
        public Uri Retina { get; set; } = null;
        public Uri Desktop { get; set; } = null;
        public Uri Tablet { get; set; } = null;
        public Uri HiResMobile { get; set; } = null;
        public Uri Mobile { get; set; } = null;
        public Uri Original { get; set; } = null;
        public Uri Thumbnail { get; set; } = null;
    }
}
