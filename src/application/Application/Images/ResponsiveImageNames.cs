﻿namespace AAE.RESS.Application.Images
{
    public enum ResponsiveImageNames
    {
        // taken from https://medium.com/hceverything/applying-srcset-choosing-the-right-sizes-for-responsive-images-at-different-breakpoints-a0433450a4a3
        FullHd,  // 1920px
        Retina,  // 1600px 1600 desktops, ipads asking for 2x
        Desktop, // 1366px - most prevalent desktop
        Tablet,    // 1024px
        HiResMobile,  // 768 px  - 2x on mobile
        Mobile,         // 640px
        Thumbnail       // 320px
    }
}
