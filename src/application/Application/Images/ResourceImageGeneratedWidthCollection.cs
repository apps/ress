﻿using System.Collections.Generic;

namespace AAE.RESS.Application.Images
{
    public class ResourceImageGeneratedWidthCollection : Dictionary<ResponsiveImageNames, short>, IDictionary<ResponsiveImageNames, short>, IResourceImageGeneratedWidthCollection
    {
        public ResourceImageGeneratedWidthCollection()
        {

        }

        public ResourceImageGeneratedWidthCollection(IEnumerable<KeyValuePair<ResponsiveImageNames, short>> collection)
            : base(collection)
        {

        }

        /// <inheritdoc />
        public ICollection<ResponsiveImageNames> Names => Keys;

    }
}
