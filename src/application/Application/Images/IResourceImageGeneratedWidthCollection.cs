﻿using System.Collections.Generic;

namespace AAE.RESS.Application.Images
{
    public interface IResourceImageGeneratedWidthCollection : IDictionary<ResponsiveImageNames, short>
    {
        /// <summary>
        /// Gets a collection of all the names of responsive images
        /// </summary>
        /// <returns></returns>
        ICollection<ResponsiveImageNames> Names { get; }
    }
}
