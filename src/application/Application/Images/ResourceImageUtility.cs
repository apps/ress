﻿using AAE.Amazon.Util;
using AAE.RESS.Config;
using System;
using System.IO;
using System.Reflection;

namespace AAE.RESS.Application.Images
{
    /// <summary>
    /// Utility to work with resource images
    /// </summary>
    public class ResourceImageUtility : IResourceImageUtility
    {
        private readonly IAmazonS3NameUtility _s3NameUtility;

        public IResourceImageGeneratedWidthCollection ResourceImageGeneratedWidths { get; }

        public ResourceImageUtility(AmazonWebServiceConfigUtility awsConfigUtility, S3Config s3Config, IResourceImageGeneratedWidthCollection imageWidths)
        {
            _s3NameUtility = new AmazonS3NameUtility(s3Config.RESSBucketName, awsConfigUtility.Hostname);
            ResourceImageGeneratedWidths = imageWidths;
        }

        /// <inheritdoc />
        public Uri GetUri(string filename, string subfolder = "")
        {
            return new Uri($"https://{_s3NameUtility.FileObjectUrl(filename, subfolder)}");
        }

        /// <inheritdoc />
        public string GenerateFilenameWithSize(string originalFilename, short width)
        {
            return $"{Path.GetFileNameWithoutExtension(originalFilename)}-{width}w{Path.GetExtension(originalFilename)}";
        }

        /// <inheritdoc />
        public ImageUriList GenerateUriList(string filename, string subfolder = "")
        {
            if (!string.IsNullOrEmpty(filename))
            {
                short width;
                string generatedFilename;
                var urlList = new ImageUriList();
                var urlListType = urlList.GetType();

                // now upload each variation in size
                foreach (var name in ResourceImageGeneratedWidths.Names)
                {
                    width = ResourceImageGeneratedWidths[name];
                    generatedFilename = GenerateFilenameWithSize(filename, width);

                    // use reflection to update the property
                    var prop = urlListType.GetProperty(name.ToString(), BindingFlags.Public | BindingFlags.Instance);
                    var value = Convert.ChangeType(GetUri(generatedFilename, subfolder), prop.PropertyType);
                    if (prop != null && prop.CanWrite)
                    {
                        prop.SetValue(urlList, value, null);
                    }
                }

                // set the original
                urlList.Original = GetUri(filename, subfolder);

                // return the completed list
                return urlList;
            }

            // no filename - return null
            return null;
        }
    }
}
