using AAE.RESS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Application.ReadModels
{
    /// <summary>
    /// Read model for listing a resource
    /// </summary>
    public class ResourceListing : IResourceListing
    {

        public ResourceType ResourceType { get; set; }
        public string ResourceSubType { get; set; }
        public short ResourceSubTypeSortIndex { get; set; }
        public string DisplayName { get; set; }
        public int Id { get; set; }
        public string Info { get; set; }
        public string Url { get; set; }
        public bool CanReserve { get; set; }
        public IResourceImage CoverImage { get; set; }
    }
}
