using AAE.RESS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Application.ReadModels
{
    /// <summary>
    /// Read model for a equipment
    /// </summary>
    public class Equipment : IResource
    {
        public Equipment()
        {
            Images = new List<IResourceImage>();
        }

        public int Id { get; set; }
        public int EquipmentTypeID { get; set; }
        public string EquipmentType { get; set; }
        public string Url { get; set; }
        string IResource.Office365EmailAddress { 
            get { 
                return O365Email;
            } 
            set
            {
                O365Email = value;
            }
        }
        string IResource.Office365ICSUrl {
            get            {
                return O365ICS;
            }
            set
            {
                O365ICS = value;
            }
        }
        public string O365Email { get; set; }
        public string O365ICS { get; set; }
        public string FullName { get; set; }
        string IResource.DisplayName { get { return FullName; } set { FullName = value; } }
        public string AbbreviatedName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public bool CanReserve { get; set; }
        public IResourceImage MapImage { get; set; }
        ResourceType IResource.ResourceType { get { return ResourceType.Equipment; } set { throw new NotImplementedException(); } }
        public IList<IResourceImage> Images { get; set; }
    }
}
