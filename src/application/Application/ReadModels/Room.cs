using AAE.RESS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Application.ReadModels
{
    /// <summary>
    /// Read model for a room
    /// </summary>
    public class Room : IResource
    {
        public Room()
        {
            Images = new List<IResourceImage>();
        }

        public int Id { get; set; }
        public int RoomTypeID { get; set; }
        public string RoomType { get; set; }
        public string Url { get; set; }
        string IResource.Office365EmailAddress { 
            get { 
                return O365Email;
            } 
            set
            {
                O365Email = value;
            }
        }
        string IResource.Office365ICSUrl {
            get            {
                return O365ICS;
            }
            set
            {
                O365ICS = value;
            }
        }
        public string O365Email { get; set; }
        public string O365ICS { get; set; }
        public string RoomNumber { get; set; }
        public string BuildingName { get; set; }
        public string DisplayName { get; set; }
        public string BuildingNumber { get; set; }
        public string Capacity { get; set; }
        public string Description { get; set; }
        public bool CanReserve { get; set; }
        public bool ShowDetails { get; set; }
        public IResourceImage MapImage { get; set; }
        ResourceType IResource.ResourceType { get { return ResourceType.Room; } set { throw new NotImplementedException(); } }
        public IList<IResourceImage> Images { get; set; }
    }
}
