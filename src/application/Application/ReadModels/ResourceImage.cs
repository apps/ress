using AAE.RESS.Application.Images;
using AAE.RESS.Interfaces;
using System.Text.Json.Serialization;

namespace AAE.RESS.Application.ReadModels
{
    /// <summary>
    /// Image used for resource
    /// </summary>
    public class ResourceImage : IResourceImage
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Filename { get; set; }
        public short Width { get; set; }
        public short Height { get; set; }
        public ImageUriList Uri { get; set; }

        [JsonIgnore]
        public string Subfolder { get; set; } = string.Empty;
    }
}
