using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AAE.Amazon.Util
{
    /// <summary>
    /// Utility for interacting with S3 file storage
    /// </summary>
    public class AmazonS3FileUtility : IAmazonS3FileUtility
    {
        private readonly IAmazonS3 _s3Client;
        private readonly ILogger<AmazonS3FileUtility> _logger;

        public AmazonS3FileUtility(ILogger<AmazonS3FileUtility> logger, IAmazonS3 s3Client)
        {
            _s3Client = s3Client ?? throw new ArgumentNullException(nameof(s3Client));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        }

        /// <summary>
        /// Uploads file to S3
        /// </summary>
        public void UploadFile(string key, IAmazonS3File file, string bucketName)
        {
            UploadFileAsync(key, file, bucketName).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Uploads file to S3
        /// </summary>
        public async Task UploadFileAsync(string key, IAmazonS3File file, string bucketName)
        {
            var fileTransferUtility = new TransferUtility(_s3Client);

            try
            {
                using (var ms = new MemoryStream())
                {
                    ms.Write(file.Data, 0, file.Data.Length);
                    ms.Position = 0;
                    var req = new TransferUtilityUploadRequest()
                    {
                        BucketName = bucketName,
                        Key = key,
                        InputStream = ms,
                        ContentType = file.MimeType,
                        CannedACL = S3CannedACL.PublicRead,
                        StorageClass = S3StorageClass.Standard
                    };


                    _logger.LogDebug($"Uploading {key} to {bucketName}");
                    await fileTransferUtility.UploadAsync(req);

                }
            }
            catch (AmazonS3Exception ex)
            {
                _logger.LogError($"Error occurred during S3 upload: Message {ex.Message}");
                throw;
            }

        }


        /// <summary>
        /// Removes the file specified file from file storage
        /// </summary>
        public void DeleteFile(string key, string bucketName)
        {
            DeleteFileAsync(key, bucketName).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Removes the file specified file from file storage
        /// </summary>
        public async Task DeleteFileAsync(string key, string bucketName)
        {

            try
            {
                var deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = bucketName,
                    Key = key
                };

                _logger.LogDebug($"Deleting {key} from {bucketName}");
                await _s3Client.DeleteObjectAsync(deleteObjectRequest);
            }
            catch (AmazonS3Exception e)
            {
                _logger.LogError($"Error encountered on server. Message:'{0}' when deleting {key}", e.Message);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError($"Unknown encountered on server. Message:'{0}' when deleting {key}", e.Message);
                throw;
            }
        }
    }
}
