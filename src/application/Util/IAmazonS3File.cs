using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.Amazon.Util
{
    /// <summary>
    /// Interface for a file to be uploaded to S3
    /// </summary>
    public interface IAmazonS3File
    {
        string MimeType { get; set; }
        
        public byte[] Data { get; set; }
    }
}
