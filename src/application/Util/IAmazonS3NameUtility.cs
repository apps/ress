﻿namespace AAE.Amazon.Util
{
    /// <summary>
    /// Interface for dealing with an S3 bucket naming
    /// </summary>
    public interface IAmazonS3NameUtility
    {
        /// <summary>
        /// Constructs the S3 key for storing the file object
        /// </summary>
        string ConstructKey(string filename, string subfolder);

        /// <summary>
        /// Returns the URL for the specified filename and subfolder
        /// </summary>
        string FileObjectUrl(string filename, string subfolder);


        /// <summary>
        /// Returns the URL for the specified file object key
        /// </summary>
        /// <param name="key">S3 file object key</param>
        string FileObjectUrl(string key);
    }
}