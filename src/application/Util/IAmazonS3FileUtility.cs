﻿using System.Threading.Tasks;

namespace AAE.Amazon.Util
{
    public interface IAmazonS3FileUtility
    {
        /// <summary>
        /// Removes the file specified file from file storage
        /// </summary>
        Task DeleteFileAsync(string key, string bucketName);

        /// <summary>
        /// Removes the file specified file from file storage
        /// </summary>
        void DeleteFile(string key, string bucketName);

        /// <summary>
        /// Uploads file to S3
        /// </summary>
        void UploadFile(string key, IAmazonS3File file, string bucketName);

        /// <summary>
        /// Uploads file to S3
        /// </summary>
        Task UploadFileAsync(string key, IAmazonS3File file, string bucketName);
    }
}