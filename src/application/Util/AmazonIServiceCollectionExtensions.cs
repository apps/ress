using Amazon.S3;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AAE.Amazon.Util
{
    public static class AmazonIServiceCollectionExtensions
    {
        /// <summary>
        /// Internal Amazon utilities to help with S3 interaction
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddAmazonUtilities(this IServiceCollection services)
        {
            services.AddAWSConfigUtility();
            services.AddScoped<IAmazonS3FileUtility, AmazonS3FileUtility>();

            return services;
        }

        /// <summary>
        /// Adds the AWS Config Utility to the DI. The Config Utility is not created until it is requested
        /// </summary>
        /// <remarks></remarks>
        private static IServiceCollection AddAWSConfigUtility(this IServiceCollection services)
        {
            // the Config Utility needs access to the IAmazonService, IAmazonS3 in this case.
            // That service is only created upon use, and is a Singleton.  In order to keep it from creating until 
            // it is ACTUALLY used, we will create a function to grab the service only when
            // our Config Utility is used

            // creating a method delegate on the fly to retrieve the IAmazonS3 service from DI and inject it into the Config Utility
            // https://docs.microsoft.com/en-us/dotnet/api/system.func-2
            //
            // Should probably create a Factory class to do this - see Amazon SDK for example
            //https://github.com/aws/aws-sdk-net/blob/master/extensions/src/AWSSDK.Extensions.NETCore.Setup/ClientFactory.cs
            static object utilFactory(IServiceProvider provider) => new AmazonWebServiceConfigUtility(provider.GetService<IAmazonS3>().Config);
            var descriptor = new ServiceDescriptor(typeof(AmazonWebServiceConfigUtility), utilFactory, ServiceLifetime.Singleton);
            services.Add(descriptor);

            return services;
        }
    }
}
