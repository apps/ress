using Amazon;
using Amazon.Runtime;
using System;
using static Amazon.RegionEndpoint;

namespace AAE.Amazon.Util
{
    /// <summary>
    /// Utility for working with an <see cref="IAmazonService"/> <see cref="IClientConfig"/>
    /// </summary>
    public class AmazonWebServiceConfigUtility
    {
        public AmazonWebServiceConfigUtility(IClientConfig clientConfig)
        {
            ClientConfig = clientConfig ?? throw new ArgumentNullException(nameof(clientConfig));
        }

        public IClientConfig ClientConfig { get; }

        /// <summary>
        /// Gets the RegionEndpoint property. The region constant to use that determines
        ///     the endpoint to use. If this is not set then the client will fallback to the
        ///     value of ServiceURL.
        /// </summary>
        public RegionEndpoint RegionEndpoint
        {
            get
            {
                return ClientConfig.RegionEndpoint;
            }
        }

        /// <summary>
        /// Gets the endpoint for a service in the region for this client config
        /// </summary>
        public Endpoint EndpointForService
        {
            get
            {
                return RegionEndpoint.GetEndpointForService(ClientConfig.RegionEndpointServiceName);
            }
        }

        /// <summary>
        /// Gets the hostname for the service in this client config
        /// </summary>
        public string Hostname
        {
            get
            {
                return EndpointForService.Hostname;
            }
        }

    }
}
