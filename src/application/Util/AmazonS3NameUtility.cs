﻿using System;
using System.Net;
using System.Text;

namespace AAE.Amazon.Util
{
    /// <summary>
    /// Utility for working with an S3 bucket naming
    /// </summary>
    public class AmazonS3NameUtility : IAmazonS3NameUtility
    {
        public string BucketName { get; }
        public string Hostname { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bucketName">S3 Bucket name where syllabi are stored</param>
        /// <param name="hostName">The hostname of the region's endpoint for service</param>
        public AmazonS3NameUtility(string bucketName, string hostName)
        {
            BucketName = bucketName;
            Hostname = hostName;
        }

        /// <inheritdoc />
        public string ConstructKey(string filename, string subfolder)
        {
            if (string.IsNullOrEmpty(filename))
                throw new ArgumentNullException(nameof(filename));

            return (!string.IsNullOrEmpty(subfolder) ? $"{subfolder}/" : "") + filename;
        }

        /// <inheritdoc />
        public string FileObjectUrl(string filename, string subfolder)
        {
            if (string.IsNullOrEmpty(filename))
                throw new ArgumentNullException(nameof(filename));

            return FileObjectUrl(ConstructKey(filename, subfolder));
        }

        /// <inheritdoc />
        public string FileObjectUrl(string key)
        {
            var sb = new StringBuilder();

            // bucket name
            sb.Append(BucketName);
            sb.Append('.');
            sb.Append(Hostname);
            sb.Append('/');
            sb.Append(WebUtility.UrlEncode(key));

            return sb.ToString();
        }


    }
}
