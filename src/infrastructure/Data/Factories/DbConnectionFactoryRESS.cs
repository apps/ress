﻿using AAE.RESS.Interfaces;
using System.Data;
using Microsoft.Data.SqlClient;

namespace AAE.RESS.Infrastructure.Data.Factories
{
    /// <summary>
    /// Connection factory for RESS DB
    /// </summary>
    public class DbConnectionFactoryRESS : DbConnectionFactoryBase, IDbConnectionFactoryRESS
    {
        public DbConnectionFactoryRESS(string connection_string) : base(connection_string)
        {

        }

        protected override IDbConnection GetConnection(string connection_string = "")
        {
            return new SqlConnection(connection_string);
        }
    }
}
