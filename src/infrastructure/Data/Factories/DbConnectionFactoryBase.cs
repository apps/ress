﻿using AAE.RESS.Interfaces;
using System.Data;

namespace AAE.RESS.Infrastructure.Data.Factories
{
    public abstract class DbConnectionFactoryBase : IDbConnectionFactory
    {

        private readonly string _connection_string;
        public DbConnectionFactoryBase(string connection_string)
        {
            _connection_string = connection_string;
        }

        public IDbConnection GetDbConnection()
        {
            return GetConnection(_connection_string);
        }

        protected abstract IDbConnection GetConnection(string connection_string = "");
    }
}
