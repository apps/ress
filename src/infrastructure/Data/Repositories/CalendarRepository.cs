using AAE.RESS.Application;
using AAE.RESS.Interfaces;
using Dapper;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AAE.RESS.Infrastructure.Data.Repositories
{
    public class CalendarRepository : ICalendarRepository
    {
        private readonly ILogger _logger;
        private readonly IDbConnectionFactory _dbConnFactory;
        private readonly IHttpClientFactory _httpClientFactory;
        public CalendarRepository(ILogger<CalendarRepository> logger, IDbConnectionFactoryRESS dbConnectionFactory, IHttpClientFactory httpClientFactory)
        {
            // check preconditions
            _logger = logger ?? throw new ArgumentNullException("logger");
            _dbConnFactory = dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IList<CalendarEvent>> GetEventsAsync(ResourceType type, string resourceUrl)
        {
            //var start = DateTime.Now.AddMonths(-6);
            //var end = DateTime.Now.AddMonths(6);

            using (var conn = _dbConnFactory.GetDbConnection())
            {
                conn.Open();

                string table = string.Empty;
                if (type == ResourceType.Equipment)
                    table = "asset";
                if (type == ResourceType.Room)
                    table = "room";

                string sql = @$"SELECT o365_ics AS O365Ics, show_details AS ShowDetails FROM {table} WHERE url=@url;";

                var result = await conn.QueryFirstOrDefaultAsync<CalenderInfo>(sql, new { url = resourceUrl }).ConfigureAwait(false);

                if (result == null) throw new Exception($"No resource found for url {type} {resourceUrl}");

                if (string.IsNullOrEmpty(result.O365Ics))
                {
                    throw new Exception($"No ics url for {type} {resourceUrl}");
                }

                using (var client = _httpClientFactory.CreateClient())
                {
                    var productValue = new ProductInfoHeaderValue("AAE-Ress", "1.0");
                    var commentValue = new ProductInfoHeaderValue("(+https://ress.aae.wisc.edu)");
                    client.DefaultRequestHeaders.UserAgent.Add(productValue);
                    client.DefaultRequestHeaders.UserAgent.Add(commentValue);

                    var response = await client.GetAsync(result.O365Ics);
                    var ics = await response.Content.ReadAsStringAsync();

                    var calendar = Calendar.Load(ics);

                    var events =  calendar.Events.Distinct().ToList();

                    if (!result.ShowDetails)
                    {
                        // do not show details - replace the summary and description with nothing.
                        events.ForEach(e =>
                        {
                            e.Summary = "Busy";
                            e.Description = "";
                        });
                    }

                    return events;
                }
            }

        }
    }

    public class CalenderInfo
    {
        public string O365Ics { get; set; }
        public bool ShowDetails { get; set; }
    }
}
