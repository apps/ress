using AAE.RESS.Interfaces;
using Dapper;
using Microsoft.Extensions.Logging;
using AAE.RESS.Security.Identity;
using System.Threading.Tasks;

namespace AAE.RESS.Infrastructure.Data.Repositories
{
    public class RESSUserRepository : IRESSUserRepository
    {
        private readonly IDbConnectionFactoryRESS _dbConnFactory;
        private readonly ILogger<RESSUserRepository> _logger;

        public RESSUserRepository(IDbConnectionFactoryRESS dbConnectionFactory, ILogger<RESSUserRepository> logger)
        {
            _logger = logger;
            _dbConnFactory = dbConnectionFactory;
        }


        public async Task<RESSUserInfo> GetAsync(int user_id)
        {
            _logger.LogDebug("Getting user by user_id={0}", user_id);

            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();
                string sql = @"SELECT id AS userID, ISNULL(last_name, username) AS lastName, ISNULL(first_name, username) AS firstName, username, 
                    ISNULL(email, username), unique_id AS uniqueID FROM [user] WHERE id=@user_id;";
                _logger.LogTrace(sql);

                RESSUserInfo user = await connection.QuerySingleOrDefaultAsync<RESSUserInfo>(sql, new { user_id });
                if (user == null)
                {
                    return RESSUserInfo.NOTFOUND;
                }
                return user;
            }
        }


        public async Task<RESSUserInfo> GetAsync(string username)
        {
            _logger.LogDebug("Getting user by username={0}", username);

            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();

                string sql = @"SELECT id AS userID, ISNULL(last_name, username) AS lastName, ISNULL(first_name, username) AS firstName, username, 
                    ISNULL(email, username), unique_id AS uniqueID FROM [user] WHERE username=@username;";
                _logger.LogTrace(sql);

                RESSUserInfo user = await connection.QuerySingleOrDefaultAsync<RESSUserInfo>(sql, new { username });
                if (user == null)
                {
                    return RESSUserInfo.NOTFOUND;
                }
                return user;
            }
        }
        public async Task<RESSUserInfo> GetAsyncByUniqueID(string unique_id)
        {
            _logger.LogDebug("Getting user by unique_id={0}", unique_id);
            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();
                string sql = @"SELECT id AS userID, ISNULL(last_name, username) AS lastName, ISNULL(first_name, username) AS firstName, username, 
                    ISNULL(email, username), unique_id AS uniqueID FROM [user] WHERE unique_id=@unique_id;";

                _logger.LogTrace(sql);
                RESSUserInfo user = await connection.QuerySingleOrDefaultAsync<RESSUserInfo>(sql, new { unique_id });
                if (user == null)
                {
                    return RESSUserInfo.NOTFOUND;
                }
                return user;
            }
        }

        /// <summary>
        /// Adds user to the persistence store
        /// </summary>
        /// <returns>Persistence unique key</returns>
        public async Task<int> AddAsync(RESSUserInfo user)
        {

            _logger.LogDebug("Adding user to persistence store: {0}", user.ToString());

            string sql = @"INSERT INTO [user] (unique_id, username, first_name, last_name, email) VALUES (@uniqueID, @username, @firstname, @lastname, @email);
                SELECT CAST(SCOPE_IDENTITY() AS int);";
            _logger.LogTrace(sql);

            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();
                return await connection.ExecuteScalarAsync<int>(sql, user);
            }
        }

        /// <summary>
        /// Update user to the persistence store
        /// </summary>
        public async Task UpdateAsync(RESSUserInfo user)
        {

            _logger.LogDebug("Updating user in persistence store: {0}", user.ToString());

            string sql = @"UPDATE [user] SET username=@username, first_name=@firstname, last_name=@lastname, email=@email, lastsync_time=sysutcdatetime() WHERE unique_id=@uniqueID;";
            _logger.LogTrace(sql);
            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();
                await connection.ExecuteAsync(sql, user);
            }
        }
    }
}
