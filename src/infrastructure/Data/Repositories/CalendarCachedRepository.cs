using AAE.Caching;
using AAE.RESS.Application;
using AAE.RESS.Interfaces;
using Ical.Net.CalendarComponents;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AAE.RESS.Infrastructure.Data.Repositories
{
    public class CalendarCachedRepository<T> : ICalendarRepository
        where T : ICalendarRepository
    {
        private readonly ICache<IList<CalendarEvent>> _cache;
        private readonly ICalendarRepository _inner;
        private readonly ILogger _logger;
        //TODO: adjust caching for production
        private readonly TimeSpan _cacheExpiration = TimeSpan.FromMilliseconds(500);

        /// <summary>
        /// Initializes a new instance of the <see cref="CalendarCachedRepository{T}"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        /// <param name="inner">The inner.</param>
        /// <param name="cache">The cache.</param>
        /// <param name="logger">The logger.</param>
        public CalendarCachedRepository(T inner, ICache<IList<CalendarEvent>> cache, ILogger<CalendarCachedRepository<T>> logger)
        {
            _inner = inner;
            _cache = cache;
            _logger = logger;
        }

        public async Task<IList<CalendarEvent>> GetEventsAsync(ResourceType type, string resourceUrl)
        {
            var events = await _cache.GetAsync($"{type}-{resourceUrl}",
                    _cacheExpiration,
                    () => _inner.GetEventsAsync(type, resourceUrl),
                    _logger);

            return events;
        }
    }
}
