using AAE.Amazon.Util;
using AAE.RESS.Application;
using AAE.RESS.Application.Images;
using AAE.RESS.Application.ReadModels;
using AAE.RESS.Config;
using AAE.RESS.Interfaces;
using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Infrastructure.Data.Queries
{
    public class RoomQueries : IRoomQueries
    {
        private readonly IDbConnectionFactory _dbConnFactory;
        private readonly ILogger<RoomQueries> _logger;
        private readonly IResourceImageUtility _imageUtility;

        public RoomQueries(IDbConnectionFactoryRESS dbConnectionFactory, ILogger<RoomQueries> logger, IResourceImageUtility imageUtility)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _dbConnFactory = dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));
            _imageUtility = imageUtility;
        }

        /// <summary>
        /// Return the room for the specified primary key
        /// </summary>
        public async Task<Room> GetAsync(int roomID)
        {
            string sql = RoomSQLSelect() + @"
					WHERE [room].[id] = @roomID
                    ORDER by [room].id, image_sort_index;";

            var param = new DynamicParameters(new { roomID });

            return (await CommonGetAsync(sql, param)).FirstOrDefault();
        }

        /// <summary>
        /// Return the room for the specified url
        /// </summary>
        public async Task<Room> GetAsync(string url)
        {

            string sql = RoomSQLSelect() + @"
						WHERE [room].[url] = @url
                        ORDER by [room].id, image_sort_index;";

            var param = new DynamicParameters(new { url });

            return (await CommonGetAsync(sql, param)).FirstOrDefault();

        }

        /// <summary>
        /// Returns all active rooms
        /// </summary>
        public async Task<IList<IResourceListing>> GetActiveListingAsync()
        {

            string sql = @"SELECT [room].Id	
	                        ,'Capacity: ' + [room].[capacity] AS [Info]
	                        ,[room].display_name AS [DisplayName]
	                        ,[room].[url] As [Url]
                            ,[room].[can_reserve] As [CanReserve]
                            ,[room].[show_details] As [ShowDetails]
	                        ,[room_type].[descr] AS ResourceSubType
	                        ,[room_type].sort_index AS ResourceSubTypeSortIndex
                            ,[image].id AS imageID  --ONLY USED FOR Dapper splitOn
                            ,[image].id
                            ,CAST([image].[guid] AS char(36)) AS [guid]
                            ,[image].[filename]
                            ,[image].[subfolder]
                            ,[image].width
                            ,[image].height
                            ,[image].mime_type AS MimeType
	                        FROM [room]
		                        INNER JOIN [room_type] ON [room].[room_type_id]=[room_type].[id]
		                        LEFT OUTER JOIN [image] ON [room].[cover_image_id] = [image].[id]
						WHERE [room].[is_active] = 1
                        ORDER by [room_type].[sort_index]";


            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();

                var rooms = (await connection.QueryAsync<ResourceListing, ResourceImage, ResourceListing>(sql,
                    (r, img) =>
                    {
                        if (img.Id != default)
                        {
                            img.Uri = _imageUtility.GenerateUriList(img.Filename, img.Subfolder);
                            r.CoverImage = img;
                        }
                            
                        r.ResourceType = ResourceType.Room;
                        return r;
                        
                    },
                    splitOn: "imageID")
                    );

                return new List<IResourceListing>(rooms);
            }

        }

        /// <summary>
        /// Common method that actual does the retrieving from the persistence store
        /// </summary>
        private async Task<IEnumerable<Room>> CommonGetAsync(string sql, DynamicParameters param)
        {
            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();

                var roomDict = new Dictionary<int, Room>();

                var rooms = (await connection.QueryAsync<Room, ResourceImage, ResourceImage, Room>(sql,
                    (r, img, mapimg) =>
                    {

                        if (!roomDict.TryGetValue(r.Id, out Room roomCached))
                        {
                            // hasn't been saved to the dict yet
                            roomCached = r;

                            if (mapimg.Id != default)
                            {
                                mapimg.Uri = _imageUtility.GenerateUriList(mapimg.Filename, mapimg.Subfolder);
                                roomCached.MapImage = mapimg;
                            }
                                

                            // now add it to the dictionary
                            roomDict.Add(r.Id, roomCached);
                        }

                        if (img.Id != default)
                        {
                            img.Uri = _imageUtility.GenerateUriList(img.Filename, img.Subfolder);
                            roomCached.Images.Add(img);
                        }
                            

                        return roomCached;
                    },
                    param: param,
                    splitOn: "imageID, mapImageID")
                    ).Distinct();

                return rooms;
            }
        }

        /// <summary>
        /// Returns the room sql SELECT statement
        /// </summary>
        private string RoomSQLSelect()
        {
            return @"SELECT [room].[id]
                      ,[room].[room_type_id] AS roomTypeID
                      ,[room].[url]
                      ,[room].[o365_email] AS o365Email
                      ,[room].[o365_ics] AS o365ICS
                      ,[room].[room_number] AS roomNumber
                      ,[room].[building_name] AS buildingName
                      ,[room].[display_name] AS displayName
                      ,[room].[building_number] AS buildingNumber
                      ,[room].[capacity]
                      ,[room].[descr] AS [Description]
                      ,[room].[can_reserve] As [CanReserve]
                      ,[room].[show_details] As [ShowDetails]
                      ,[map_image_id] AS mapImageID
                      ,[cover_image_id] AS coverImageID
	                  ,[room_type].descr AS roomType
                      ,[room_image].sort_index AS image_sort_index
                      ,[image].id AS imageID  --ONLY USED FOR Dapper splitOn
                      ,[image].id
                      ,CAST([image].[guid] AS char(36)) AS [guid]
                      ,[image].[filename]
                      ,[image].[subfolder]
                      ,[image].width
                      ,[image].height
                      ,[image].mime_type AS MimeType
                      ,[map_image].id AS mapImageID  --ONLY USED FOR Dapper splitOn
                      ,[map_image].id
                      ,CAST([map_image].[guid] AS char(36)) AS [guid]
                      ,[map_image].[filename]
                      ,[map_image].[subfolder]
                      ,[map_image].width
                      ,[map_image].height
                      ,[map_image].mime_type AS MimeType
                  FROM [room]
	                INNER JOIN [room_type] ON room.room_type_id=room_type.id
                    LEFT OUTER JOIN [room_image] ON room.id = room_image.room_id
                    LEFT OUTER JOIN [image] ON [room_image].image_id = [image].id
                    LEFT OUTER JOIN [image] AS [map_image] ON [room].[map_image_id] = [map_image].id";
        }
    }
}
