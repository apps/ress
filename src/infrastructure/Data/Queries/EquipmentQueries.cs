using AAE.RESS.Application;
using AAE.RESS.Application.Images;
using AAE.RESS.Application.ReadModels;
using AAE.RESS.Interfaces;
using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AAE.RESS.Infrastructure.Data.Queries
{
    public class EquipmentQueries : IEquipmentQueries
    {
        private readonly IDbConnectionFactory _dbConnFactory;
        private readonly ILogger<EquipmentQueries> _logger;
        private readonly IResourceImageUtility _imageUtility;

        public EquipmentQueries(IDbConnectionFactoryRESS dbConnectionFactory, ILogger<EquipmentQueries> logger, IResourceImageUtility imageUtility)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _dbConnFactory = dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));
            _imageUtility = imageUtility ?? throw new ArgumentNullException(nameof(imageUtility));

        }

        /// <summary>
        /// Return the equipment for the specified primary key
        /// </summary>
        public async Task<Equipment> GetAsync(int equipmentID)
        {
            string sql = EquipmentSQLSelect() + @"
					WHERE [asset].[id] = @equipmentID
                    ORDER by [asset].id, image_sort_index;";

            var param = new DynamicParameters(new { equipmentID });

            return (await CommonGetAsync(sql, param)).FirstOrDefault();
        }

        /// <summary>
        /// Return the equipment for the specified url
        /// </summary>
        public async Task<Equipment> GetAsync(string url)
        {

            string sql = EquipmentSQLSelect() + @"
						WHERE [asset].[url] = @url
                        ORDER by [asset].id, image_sort_index;";

            var param = new DynamicParameters(new { url });

            return (await CommonGetAsync(sql, param)).FirstOrDefault();

        }

        /// <summary>
        /// Returns all active equipment
        /// </summary>
        public async Task<IList<IResourceListing>> GetActiveListingAsync()
        {

            string sql = @"SELECT [asset].Id	
	                        ,'' AS [Info]
	                        ,[asset].full_name AS [DisplayName]
	                        ,[asset].[url] As [Url]
                            ,[asset].[can_reserve] As [CanReserve]
	                        ,[asset_type].[descr] AS ResourceSubType
	                        ,[asset_type].sort_index AS ResourceSubTypeSortIndex
                            ,[image].id AS imageID  --ONLY USED FOR Dapper splitOn
                            ,[image].id
                            ,CAST([image].[guid] AS char(36)) AS [guid]
                            ,[image].[filename]
                            ,[image].[subfolder]
                            ,[image].width
                            ,[image].height
                            ,[image].mime_type AS MimeType
	                        FROM [asset]
		                        INNER JOIN [asset_type] ON [asset].[asset_type_id]=[asset_type].[id]
		                        LEFT OUTER JOIN [image] ON [asset].[cover_image_id] = [image].[id]
						WHERE [asset].[is_active] = 1
                        ORDER by [asset_type].[sort_index]";


            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();

                var equipment = (await connection.QueryAsync<ResourceListing, ResourceImage, ResourceListing>(sql,
                    (a, img) =>
                    {
                        if (img.Id != default)
                        {
                            img.Uri = _imageUtility.GenerateUriList(img.Filename, img.Subfolder);
                            a.CoverImage = img;
                        }
                            
                        a.ResourceType = ResourceType.Equipment;
                        return a;

                    },
                    splitOn: "imageID")
                    );

                return new List<IResourceListing>(equipment);
            }

        }

        /// <summary>
        /// Common method that actual does the retrieving from the persistence store
        /// </summary>
        private async Task<IEnumerable<Equipment>> CommonGetAsync(string sql, DynamicParameters param)
        {
            using (var connection = _dbConnFactory.GetDbConnection())
            {
                connection.Open();

                var assetDict = new Dictionary<int, Equipment>();

                var equipment = (await connection.QueryAsync<Equipment, ResourceImage, Equipment>(sql,
                    (a, img) =>
                    {

                        if (!assetDict.TryGetValue(a.Id, out Equipment assetCached))
                        {
                            // hasn't been saved to the dict yet
                            assetCached = a;

                            // now add it to the dictionary
                            assetDict.Add(a.Id, assetCached);
                        }

                        if (img.Id != default)
                        {
                            img.Uri = _imageUtility.GenerateUriList(img.Filename, img.Subfolder);
                            assetCached.Images.Add(img);
                        }
                            

                        return assetCached;
                    },
                    param: param,
                    splitOn: "imageID")
                    ).Distinct();

                return equipment;
            }
        }

        /// <summary>
        /// Returns the equipment sql SELECT statement
        /// </summary>
        private string EquipmentSQLSelect()
        {
            return @"SELECT [asset].[id]
                      ,[asset].[asset_type_id] AS EquipmentTypeID
                      ,[asset].[url]
                      ,[asset].[o365_email] AS o365Email
                      ,[asset].[o365_ics] AS o365ICS
                      ,[asset].[full_name] AS FullName
                      ,[asset].[abbrev_name] AS AbbreviatedName
                      ,[asset].[location]
                      ,[asset].[descr] AS [Description]
                      ,[asset].[can_reserve] As [CanReserve]
                      ,[cover_image_id] AS coverImageID
	                  ,[asset_type].descr AS EquipmentType
                      ,[asset_image].sort_index AS image_sort_index
                      ,[image].id AS imageID  --ONLY USED FOR Dapper splitOn
                      ,[image].id
                      ,CAST([image].[guid] AS char(36)) AS [guid]
                      ,[image].[filename]
                      ,[image].[subfolder]
                      ,[image].width
                      ,[image].height
                      ,[image].mime_type AS MimeType
                  FROM [asset]
	                INNER JOIN [asset_type] ON asset.asset_type_id=asset_type.id
                    LEFT OUTER JOIN [asset_image] ON asset.id = asset_image.asset_id
                    LEFT OUTER JOIN [image] ON [asset_image].image_id = [image].id";
        }
    }
}
