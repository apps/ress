using AAE.RESS.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Infrastructure.Data.Queries
{
    public class TypeQueries
    {
        private readonly IDbConnectionFactory _dbConnFactory;
        private readonly ILogger<TypeQueries> _logger;

        public TypeQueries(IDbConnectionFactoryRESS dbConnectionFactory, ILogger<TypeQueries> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _dbConnFactory = dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));

        }


    }
}
