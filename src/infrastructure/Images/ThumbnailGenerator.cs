#pragma warning disable CA1416    // This call site is reachable on all platforms. 'XXXXXX' is only supported on: 'windows'
using System.IO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace AAE.RESS.Infrastructure.Images
{
    /// <summary>
    /// Generate thumbnails for images
    /// </summary>
    public static class ThumbnailGenerator
    {
        /// <summary>
        /// Generates the thumbnails for the system to use
        /// </summary>
        /// <remarks></remarks>
        public static void GenerateThumbnails(string filename, int[] sizes)
        {
            try
            {
                Image originalImage = Image.FromFile(filename);
                FileInfo fi = new FileInfo(filename);

                for (var i = 0; i < sizes.Length; i++)
                {
                    var img = ResizeImage(originalImage, sizes[i]);
                    img.Save($"{fi.Name}x{sizes[i]}{fi.Extension}");
                }

            }
            catch
            {
                throw;
            }

            
        }


        /// <summary>
        /// Resize the image so that the longest side is the specified value
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="longSide">Size of the longest side of the image</param>
        public static Bitmap ResizeImage(Image image, int longSide)
        {
            int width;
            int height;

            if (image.Width > image.Height)
            {
                width = longSide;
                height = Convert.ToInt32(image.Height * longSide / image.Width);
            }
            else
            {
                width = Convert.ToInt32(image.Width * longSide / image.Height);
                height = longSide;
            }

            return ResizeImage(image, width, height);
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
#pragma warning restore CA1416