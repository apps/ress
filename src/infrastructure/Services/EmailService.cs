using AAE.RESS.Application.Configuration;
using AAE.RESS.Application.Interfaces;
using AAE.RESS.Settings;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Threading.Tasks;

namespace AAE.RESS.Infrastructure
{
    public class EmailService : IEmailService
    {
        private readonly ILogger _logger;

        public EmailService(IOptions<MailSettings> mailSettings, ILogger<EmailService> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            MailSettings = mailSettings.Value ?? throw new ArgumentNullException(nameof(mailSettings));
        }

        public MailSettings MailSettings { get; }
        
        public Task SendAdminAsync(string subject, MimeEntity body)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(MailSettings.SenderName, MailSettings.Sender));
            mimeMessage.To.Add(new MailboxAddress(MailSettings.SysAdminEmail, MailSettings.SysAdminEmail));

            mimeMessage.Subject = subject;

            mimeMessage.Body = body;

            return SendAsync(mimeMessage);
        }

        public Task SendAsync(EmailMessage email)
        {
            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(email.From ?? new MailboxAddress(MailSettings.SenderName, MailSettings.Sender));
            mimeMessage.To.AddRange(email.To);

            mimeMessage.Subject = email.Subject;

            var builder = new BodyBuilder();

            if (!string.IsNullOrEmpty(email.TextBody))
                builder.TextBody = email.TextBody;

            if (!string.IsNullOrEmpty(email.HtmlBody))
                builder.HtmlBody = email.HtmlBody;

            mimeMessage.Body = builder.ToMessageBody();

            return SendAsync(mimeMessage);
        }

        public async Task SendAsync(MimeMessage message)
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(MailSettings.MailServer);

                    // Note: only needed if the SMTP server requires authentication
                    //await client.AuthenticateAsync(_emailSettings.Sender, _emailSettings.Password);

                    await client.SendAsync(message);

                    client.Disconnect(true);
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw;
            }

        }
    }
}
