using AAE.Identity;
using AAE.RESS.Interfaces;
using AAE.RESS.Security.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AAE.RESS.Infrastructure.Identity
{
    public class RESSUserManager : IRESSUserManager
    {
        private readonly IRESSUserRepository _userRepository;
        private readonly ILogger _logger;

        public RESSUserManager(IRESSUserRepository userRepository, ILogger<RESSUserManager> logger)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException("userRepository");
            _logger = logger ?? throw new ArgumentNullException("logger");
        }

        /// <summary>
        /// Registers a user in the app (if necessary)
        /// </summary>
        /// <returns>Primary key for the user in the app</returns>
        public async Task<int> RegisterUserInAppAsync(IApplicationUserInfo user)
        {
            var incoming_user = new RESSUserInfo(user);

            RESSUserInfo current_user = await _userRepository.GetAsyncByUniqueID(user.UniqueID);

            if (current_user == RESSUserInfo.NOTFOUND)
            {
                _logger.LogDebug("No user found in RESS for UniqueID={0}", user.UniqueID);

                //user hasn't been registered in the app yet
                return await _userRepository.AddAsync(incoming_user);
            }
            else
            {
                //compare the stored user to the supplied user
                if (!current_user.CompareTo(incoming_user))
                {
                    _logger.LogDebug("Existing user needs to be updated in persistence store -{0}", user.ToString());
                    //they have different properties - update the persistence store
                    incoming_user.UserID = current_user.UserID;
                    await _userRepository.UpdateAsync(incoming_user);
                }

                return current_user.UserID;
            }
        }
    }
}
