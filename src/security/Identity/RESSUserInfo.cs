using AAE.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace AAE.RESS.Security.Identity
{
    /// <summary>
    /// Internal-only user information from the persistence store
    /// </summary>
    public class RESSUserInfo : ApplicationUserInfo, IApplicationUserInfo
    {
        public new static RESSUserInfo NOTFOUND = new RESSUserInfo() { FirstName = "NOT", LastName = "FOUND", Username = "NOTFOUND", UniqueID = "XXX" };

        protected RESSUserInfo()
        {

        }

        /// <summary>
        /// Initialize RESS user information - leaves the UserID at default
        /// </summary>
        public RESSUserInfo(IApplicationUserInfo info) : base(info)
        {

        }

        public int UserID { get; set; } = default;

        /// <summary>
        /// Determines whether the properties of this <see cref="RESSUserInfo"/> are equal to the supplied <see cref="IApplicationUser"/>
        /// </summary>
        /// <remarks>Doesn't compare primary key (<see cref="UserID"/>)</remarks>
        public bool CompareTo(RESSUserInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            bool result = FirstName == info.FirstName
                   & LastName == info.LastName
                   & Email == info.Email
                   & Username == info.Username
                   & UniqueID == info.UniqueID;


            return result;
        }
    }
}
