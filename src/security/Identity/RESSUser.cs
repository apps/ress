using AAE.Identity;
using System;
using System.Linq;
using System.Security.Claims;

namespace AAE.RESS.Security.Identity
{
    /// <summary>
    /// <see cref="IApplicationUser"/> for the RESS app
    /// </summary>
    public class RESSUser : ClaimsApplicationUser, IApplicationUser
    {
        public const string RESSAuthenticationType = "RESS";

        public RESSUser(ClaimsIdentity identity) : base(identity)
        {
            var clUserID = identity.FindFirst(AAEClaimTypes.UserID);
            if (clUserID != null) UserID = Convert.ToInt32(clUserID.Value);

        }

        public RESSUser(ClaimsPrincipal principal) : this((ClaimsIdentity)principal.Identity)
        {

        }

        /// <summary>
        /// Overrides a userID claim (if one exists)
        /// </summary>
        public RESSUser(ClaimsPrincipal principal, int user_id) : this(principal)
        {
            UserID = user_id;
        }

        /// <summary>
        /// Overrides a userID claim (if one exists)
        /// </summary>
        public RESSUser(ClaimsIdentity identity, int user_id) : this(identity)
        {
            UserID = user_id;
        }

        public int UserID { get; protected set; } = -1;


        public override ClaimsIdentity ToClaimsIdentity(string authenticationType = null)
        {
            var ident = base.ToClaimsIdentity(RESSAuthenticationType);
            ident.AddClaim(new Claim(AAEClaimTypes.UserID, UserID.ToString()));
            return ident;
        }


        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}, {4}", Username, UniqueID, Email, FirstName, LastName);
        }

        /// <summary>
        /// Returns a <see cref="CCLogUser"/> populated from the proper identity in the supplied <see cref="ClaimsPrincipal"/>
        /// </summary>
        ///<remarks>Throws an <see cref="Exception"/> if the proper identity is not in the <see cref="ClaimsPrincipal"/></remarks>
        public static RESSUser UserFromPrincipal(ClaimsPrincipal claimsPrincipal)
        {
            var ress_identity = claimsPrincipal.Identities.Where(i => i.AuthenticationType == RESSAuthenticationType).FirstOrDefault();
            if (ress_identity == null)
                throw new Exception("No RESS identity found in the supplied ClaimsPrincipal");

            return new RESSUser(ress_identity);
        }

    }
}
