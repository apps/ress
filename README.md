# Examples of Vue

There are two main areas that Vue is being used in this app.   They are:

## The Form
See it in production: (https://ress.aae.wisc.edu/rooms/rm113/reserve)

The main locations to see the source:
*  HTML is in `Web\Views\Reserve\Index.cshtml`
*  Entry point for the Vue app is `Web\assets\javascripts\pages\reserve\index\index.js`
*  The main component is the form itself, which is at `Web\assets\javascripts\reserveform`

## The Calendar
See it in production: (https://ress.aae.wisc.edu/rooms/rm113/calendar)

The main locations to see the source:
*  HTML is in `Web\Views\Calendar\Index.cshtml`
*  Entry point for the Vue app is `Web\assets\javascripts\pages\calendar\index\index.js`
*  The main component is the form itself, which is at `Web\assets\javascripts\resource_calendar\index.js`